var TIAA_ud = TIAA_ud || {};

TIAA_ud = {
    udThemeUrl			: '/themes/ud_atom/release_2016-12/',
    globalAssetUrl		: '/assets/',
    globalThemeUrl		: '/assets/release_2015-06/',
    localUrl			: 'assets/',
    leftNavMode			: 'none', // - For Participant Summary like pages ps' || DEFAULT is 'none' - No Left nav || '$Element' - The section that needs to be on right side,
	oneColWidth			: 1080,
	twoColWidth			: 980,
	versioning			: true,
	// phpMode			: true,          // Use PHP
	phpMode			    : false,         // Use jQuery
	scenarioCheck		: 'release', 	 // For version specific scenario - enter release value
	panelToggleVisible	: true, 		 // Panel toggling
	startVersion        : 'feb17',
    cssName				: 'ud_bob.css',  // Proj Specific CSS	
    jsName				: 'ud_bob.js',   // Proj Specific JS
    projLoc				: '/ud/ibob/'    // Location of currect project from prototype root
};

TIAA_ud.releaseObj = {
    // DO NOT EDIT THE BELOW

    // All these are current default codes
    defaults: {
        proCSS: true,
        header: true,
        pagetitle: true,
        body: true,
        popups: true,
        proJS: true
    },

    s3: {
        ibob: {
            proCSS: true,
            header: true,
            pagetitle: true,
            body: true,
            popups: true,
            proJS: true
        }
    },
    s4: {
        ibob: {
            proCSS: true,
            header: true,
            pagetitle: true,
            body: true,
            popups: true,
            proJS: true,
            scenario: true
        }
    },
    s5: {
        ibob: {
            proCSS: true,
            header: true,
            pagetitle: true,
            body: true,
            popups: true,
            proJS: true,
            scenario: true
        }
    },
    s6: {
        ibob: {
            proCSS: true,
            header: true,
            pagetitle: true,
            body: true,
            popups: true,
            proJS: true,
            scenario: true
        }
    },
    s7: {
        ibob: {
            proCSS: true,
            header: true,
            pagetitle: true,
            body: true,
            popups: true,
            proJS: true,
            scenario: true
        }
    },
    s8: {
        ibob: {
            proCSS: true,
            header: true,
            pagetitle: true,
            body: true,
            popups: true,
            proJS: true,
            scenario: true
        }
    },
    so: {
        ibob: {
            proCSS: true,
            header: true,
            pagetitle: true,
            body: true,
            popups: true,
            proJS: true,
            scenario: true
        }
    },
    s10: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-03/',
			globalThemeUrl		: '/assets/release_2015-06/'
		},
        ibob: {
            body:true,	
			popups: true,
			proJS: true,
			proCSS: true,
			scenario: true,
			pagetitle: true
        }
    }
    ,   

     aug15: {
		themeUrl:{
			udThemeUrl			: '/themes/ud2_atom/release_2016-03/',
			globalThemeUrl		: '/assets/release_2015-06/'
		},
        ibob: {
            body:true,	
			popups: true,
			proJS: true,
			proCSS: true,
			scenario: true,
			pagetitle: true
        }
    }   ,   

     nov15: {
        themeUrl:{
            udThemeUrl          : '/themes/ud2_atom/release_2016-03/',
            globalThemeUrl      : '/assets/release_2016-03/'
        },
        ibob: {
            body:true,  
            popups: true,
            proJS: true,
            proCSS: true,
            scenario: true,
            pagetitle: true
        }
    },   

     july16: {
        themeUrl:{
            udThemeUrl          : '/themes/ud2_atom/release_2016-07/',
            globalThemeUrl      : '/assets/release_2016-03/'
        },
        ibob: {
            body:true,  
            popups: true,
            proJS: true,
            proCSS: true,
            scenario: true,
            pagetitle: true
        },
        scoredetails: {
            body:true,  
            popups: true,
            proJS: true,
            proCSS: true,
            scenario: true,
            pagetitle: true
        }        
    },
	
	 dec16: {
        themeUrl:{
            udThemeUrl          : '/themes/ud2_atom/release_2016-07/',
            globalThemeUrl      : '/assets/release_2016-03/'
        },
        ibob: {
            body:true,  
            popups: true,
            proJS: true,
            proCSS: true,
            scenario: true,
            pagetitle: true
        },
        scoredetails: {
            body:true,  
            popups: true,
            proJS: true,
            proCSS: true,
            scenario: true,
            pagetitle: true
        }        
    },
	
	 feb17: {
        themeUrl:{
            udThemeUrl          : '/themes/ud2_atom/release_2016-07/',
            globalThemeUrl      : '/assets/release_2016-03/'
        },
        ibob: {
            body:true,  
            popups: true,
            proJS: true,
            proCSS: true,
            scenario: true,
            pagetitle: true
        },
        scoredetails: {
            body:true,  
            popups: true,
            proJS: true,
            proCSS: true,
            scenario: true,
            pagetitle: true
        }        
    }
 
};

// Updated the protoSettings
document.write('<script src="/themes/ud_atom/templates/proto_setting.js"></script>');

// Version based scenario 
(function(cont) {
	function checkstore() {
		if (typeof store === 'undefined') {
			setTimeout(function() {
			 	checkstore();
			}, 5);
		} else {
			$(document).trigger('storeLoaded');                   
			// Code on decisions
		};
	};
	checkstore();
})(window);

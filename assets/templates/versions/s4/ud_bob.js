$(document).on('bodyContLoaded', function () {
	$('.myTab').tabs();
	
	
	
	if($('html').hasClass('ie7') == true || $('html').hasClass('ie6') == true){
		$(document).on('click','#winProb', function(e){
			e.preventDefault();
			var linkName = $(this).closest('li').find('div').eq(0).children('input').attr('id');
			var boxW = $('#infodivWin').height();
			var infoHf = (boxW/2)+10;
			var topPos = ($('#'+linkName).offset().top)-infoHf;
			var leftPos = $(this).closest('li').width(); 
			$('#infodivWin').css({'top': (topPos+10)+'px','left': (leftPos+40)+'px'}).addClass('visible').removeClass('closed');
			//$('#infodivWin').addClass('visible').removeClass('closed');
		});
		}
		/* IE 6 & 7 popup open/close starts here */	
	if($('html').hasClass('ie7') == true || $('html').hasClass('ie6') == true){
		$(document).on('click','a[href*="#"]', function(e){
		var iePopups = $(this).attr('href');
			iePopups = '#'+iePopups.substring(iePopups.lastIndexOf('#') +1);
			$(iePopups).dialog('open');
		});
	}
	/* IE 6 & 7 popup open/close ends here */	
	
		
	
	 var userInfo = store.get('user');
	   
	if(userInfo == "UWC"){
		$('#bookselect2').addClass('closed');
		$('#clientSelector').removeClass('closed');
		
		$(document).on('click','#saveCustomBook',function(e){
			if($('#radio1').is(':checked') == true){
				var bnp = $('#bookNamePopup').val(),
				bnpL = bnp.length,
				lstC = bnp.substr(0, 3),
				sdb = $('#setDfltBook').is(':checked');
				if(sdb == true){
					$('#bookselect1').append('<option value="'+bnpL+'_'+lstC+'" selected="selected">'+bnp+'</option>');
					$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
				}else{
					$('#myClients').append('<option value="'+bnpL+'_'+lstC+'">'+bnp+'</option>');
					$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
				}	
			}
			else if($('#radio2').is(':checked') == true){
				var bnp = $('#bookNamePopup').val(),
				bnpL = bnp.length,
				lstC = bnp.substr(0, 3),
				sdb = $('#setDfltBook').is(':checked');
				if(sdb == true){
					$('#bookselect1').append('<option value="'+bnpL+'_'+lstC+'" selected="selected">'+bnp+'</option>');
					$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
				}else{
					$('#allClients').append('<option value="'+bnpL+'_'+lstC+'">'+bnp+'</option>');
					$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
				}
			}else{
			
			}
			
				$('#createBookPopup').dialog('close');
		});
	}
	else{
		$('#clientSelector').addClass('closed');
		$('#bookselect1').addClass('closed');
		
		$(document).on('click','#saveCustomBook',function(e){
			var bnp = $('#bookNamePopup').val(),
			bnpL = bnp.length,
			lstC = bnp.substr(0, 3),
			sdb = $('#setDfltBook').is(':checked');
			if(sdb == true){
				$('#bookselect2').append('<option value="'+bnpL+'_'+lstC+'" selected="selected">'+bnp+'</option>');
				$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
			}else{
				$('#bookselect2').append('<option value="'+bnpL+'_'+lstC+'">'+bnp+'</option>');
				$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
			}
			$('#createBookPopup').dialog('close');
		});
	}

	$(document).on('click','#myClients', function(e){
		$('#radio1').attr('checked',true);
		
	});
	$(document).on('click','#allClients', function(e){
		$('#radio2').attr('checked',true);
		
	});
	$(document).on('click','#radio2', function(e){
		$('#allClientStates').removeClass('closed');
		$('#myClientStates').addClass('closed');
	});
		
	$(document).on('click','#radio1', function(e){
		$('#allClientStates').addClass('closed');
		$('#myClientStates').removeClass('closed');
	});
	$(document).on('click','#radio1, #radio2', function(e){
		$('.curSels').removeClass('closed');
		$('#curSelsSec').addClass('closed');
		$('.current').addClass('closed');
		$('#instCurSel').html('None');
		//$('#alertPopup').dialog('open');
	});
	//$('#cancelViewChange').
	$(document).on('change','#bookselect1 , #bookselect2', function(e){
		var bnpV = $(this).val(),
			bnp = bnpV.split('_');
		$('#curSelsSec').removeClass('closed');
		$('.selBook').html($(this).find(":selected").text());
		$('.curSels').html();
		$('#instCurSel').html(bnp[0]+' Institutions meet current selections:');
		$('#'+bnp[1]).addClass('selNlist').removeClass('closed').siblings().addClass('closed').removeClass('selNlist');
		$('.current').addClass('closed');
		$('.defaultOpt').attr('selected', 'selected');
	});
	
	
	
	$('#curSelsSec').on('click','.icdeleteIcon',function(e){
		$(this).closest('li').addClass('closed');
	});
	
	$(document).on('click','#clearAllRef',function(e){
		$('.curSels').removeClass('closed');
		$('#curSelsSec').addClass('closed');
		$('.current').addClass('closed');
		$('#instCurSel').html('None');
	});
	
	$(document).on('change','#actionSelect', function(e){
		if($(this).find(":selected").text() == "Rename"){
			$('#renameBook').removeClass('closed');
		}else{
			$('#renameBook').addClass('closed');
		}
	});
	
	$(document).on('click','#createBook', function(e){
		$('#curSelPopup').html('');
		var creatbook = $('#currentSel').html();
		console.log(creatbook);
		$('#curSelPopup').append(creatbook);
	});


	$(document).on('click','#mngCustLst li',function(e){
		$(this).addClass('shdhlt').siblings().removeClass('shdhlt');
		$('#actionSelect').removeAttr('disabled');
	});
	$(document).on('click','#applyMngCust',function(e){
		var renBk = $('#renameBook').val(),
			actSel = $('#actionSelect').find(":selected").text();
		if(actSel == "Rename"){
			if(renBk != ''){
				$('#mngCustLst').find('.shdhlt').html('<span>'+renBk+'</span>');
				var renItem = $('#mngCustLst').find('.shdhlt').attr('rel');
				$('#bookselect1').find('option[rel='+renItem+']').text(renBk);
				$('#renameBook').val('');
			}else{
				e.preventDefault();
			}
		}else if(actSel == "Set As Default Book"){
			var mcl = $('#mngCustLst').find('.shdhlt').text();
				$('#bookselect1').append('<option selected="selected">'+mcl+'</option>');
				$('#manageBookPopup').dialog('close');
		}else if(actSel == "Delete"){
			var delItem = $('#mngCustLst').find('.shdhlt').attr('rel');
			$('#mngCustLst').find('.shdhlt').addClass('closed');
			$('#bookselect1').find('option[rel='+delItem+']').remove();
			e.preventDefault();
		}else{
			e.preventDefault();
		}
		$('#manageBookPopup').dialog('close');
	});
	
	$(document).on('click','.icdeleteIcon',function(e){
		$(this).closest('li').addClass('closed');
	});
	
	$('.hd').find('h3').on('click', function(e){
		e.preventDefault();
		if($(this).closest('div').hasClass('hdexpanded') == true){
			$(this).find('a').addClass('collapsed').removeClass('expanded');
			$(this).closest('div').next().addClass('expanded bgexpanded');
		}else{
			$(this).closest('div').next().addClass('expanded bgexpanded').siblings('.content').removeClass('expanded bgexpanded').hide();
			$(this).closest('div').next().siblings('.hd').removeClass('hdexpanded').find('a').addClass('collapsed').removeClass('expanded');
		}
	});
	
	$('#leftcol_bob .secondlevel').on('click',function(e){
		if($(this).attr('checked')){
			$(this).closest('ul').parent().addClass('activeli');
			$(this).closest('content').parent().addClass('activeli');
		}
		else{
			$(this).closest('ul').parent().removeClass('activeli');	
		}
	});
	
	$(document).on('click','#channel, #lastVisited, #status', function(e){
		$('#ui-id-2').trigger('click');
		$('#tbl_ibob_interactions thead').find('th').eq(4).addClass('headerSortDown').siblings().removeClass('headerSortDown headerSortUp');
	});
	
	$(document).on('click','#opportunityType, #serviceModel, #salesPhase, #winProbability, #productAndServices', function(e){
		$('#ui-id-3').trigger('click');
	});
	
//Select all function

$(document).on('change','#selectAll', function(){
	if($(this).is(':checked')){
		$(this).closest('ul').next().find('input[type=checkbox]').prop('checked', true);
		$(this).closest('ul').next().find('input[type=checkbox]').trigger('click');
		$(this).closest('ul').next().find('input[type=checkbox]').prop('checked', true);
		$('#selectAllOpen').find('.content1').removeClass('closed');
		$('#selectAllOpen').find('.clearspan').removeClass('closed');
		$('#selectAllOpen').find('.editLink').addClass('closed');
	}else{
		$(this).closest('ul').next().find('input[type=checkbox]').prop('checked', false);
	}
});

//Select all function ends

//Parent Checkbox Selects All

$(document).on('change','.firstlevel', function(e){
	if($(this).is(':checked')){
		$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').prop('checked', true);
		$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').trigger('click');
		$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').prop('checked', true);
		$('.secondlevel').closest('ul').parent().addClass('activeli');
	}
	else{
		$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').prop('checked', false);
		$('.secondlevel').closest('ul').parent().removeClass('activeli');
	}
});
var ct = 0;
$('.secondleveldiv').find('input[type=checkbox]').on('click', function(){
	if($(this).is(":checked") == false){
		$(this).closest('.secondleveldiv').parent('li').find('.firstlevel').prop("checked", false);
	}
	//var scF = $(this).closest('.secondleveldiv').parent('li').find('.secondlevel').length;
	//$(this).closest('.secondleveldiv').find('.secondlevel').each(function(){
	//	$(this).closest('.secondleveldiv').find('input[type=checkbox]:checkbox').length;
	//		ct++;		
	//});
	//console.log(ct);
});



//First Level Functionality
$('#leftcol_bob a.editLink').on('click',function(e){
	$(this).closest('li').find('.secondleveldiv').removeClass('closed');
	$(this).closest('li').siblings().addClass('shd3').find('.content1').addClass('closed');
	$(this).closest('.content').find('li.shd3').each(function( index ) {	
		$(this).removeClass('shd3');
		$(this).find('.clearspan').addClass('closed');
		$(this).find('.editLink').removeClass('closed');
		$(this).find('.secondleveldiv li').each(function(index) {		
			if($(this).hasClass('activeli') && $(this).parent().hasClass('nlist') ){		
				$(this).removeClass('closed');
				
			}else{
				if($(this).parent().hasClass('nlist')){
				//$(this).addClass('closed');
			}
		}
	});
	
	});
		$(this).closest('li').find('.clearspan').removeClass('closed');	
		$(this).closest('li').addClass('shd3');
		$(this).closest('li').find('.secondleveldiv').removeClass('closed');
		$(this).closest('li').find('.secondleveldiv').find('.secondlevel').removeClass('hidden');
		$(this).addClass('closed');
		$(this).closest('li').find('.secondleveldiv li').each(function( index ) {	
			$(this).parent().parent().removeClass('closed');
			if( $(this).parent().hasClass('nlist')){
			 $(this).find('input[type=checkbox]').removeClass('closed');
			 $(this).removeClass('closed');
			}
		});
		$(this).closest('li').find('.firstleveldiv').find('input[type=checkbox]').prop('disabled', false);
		$(this).closest('li').find('.secondleveldiv').find('.teamScroll').removeClass('closed');
});

	
$('#leftcol_bob .icclearLink').on('click',function(e){
	$(this).parent().parent().removeClass('shd3');
	$(this).parent().parent().find('.editLink').removeClass('closed');
	$(this).parent().addClass('closed');
	
	$(this).parent().parent().find('.secondleveldiv').addClass('closed');
	var ct = 0;
	$(this).parent().parent().find('.secondleveldiv li').each(function() {	
		var chkbox = $(this).find('input[type=checkbox]').is(':checked'),
			radBtn = $(this).find('input[type=radio]').is(':checked');
		if(chkbox == true || radBtn == true){
			ct++;
		}
	});
	
	
	$(this).closest('li').find('.firstleveldiv').find('input[type=checkbox]').prop('disabled', true);
	$(this).closest('li').find('.secondleveldiv').find('.teamScroll').addClass('closed');
});

	$('.rad').on('click', function(e){
			$('.sel').remove();
			$(this).closest('ul').parent().addClass('activeli').siblings().removeClass('activeli');
			if($(this).closest('ul').parent().hasClass('activeli')== true){
				var radio = $(this).next().text();
			}
			var curr = ($(this).closest('.content').prev().text());
				$('#currentSel').append('<li class="sel"><span>'+curr+':'+radio+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
				$('.current').removeClass('closed');
			
	});


$('#winCancel').on("click", function(e){
	$('#infodivWin').addClass('closed').removeClass('visible');
});

$( "#timeframe-slider" ).slider({
		orientation: "horizontal",
		range: true,
		value:100,
      min: 0,
      max: 100,
      step: 1,
		values: [ 10,30 ],
		slide: function( event, ui ) {
				$( "#range1" ).val( ui.values[ 0 ]);
				$( "#range2" ).val( ui.values[ 1 ]);
			}
		});
	//$('#range1').val($('#range1Val').text());
	//$('#range2').val($('#range2Val').text());
			
	

$(document).on('keyup','#range1, #range2', function(){
	var $ran1 = $(this).val(),
	num = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/,
	slider = $(this).closest('.infoHover').find('.slider');
	if(!num.test($ran1) || $ran1 > 100) {	
		$(this).addClass('descl');
	}else{
		$(this).removeClass('descl');
		if($(this).attr('id') == 'range1'){
			slider.find('a').eq(0).css({'left': $ran1+'%'});
		}else{
			slider.find('a').eq(1).css({'left': $ran1+'%'});
		}
	}
	var r1 = $('#range1').val(),
		r2 = $('#range2').val();
	if(r1 > r2){
		var rng = r1-r2;
		$(this).closest('.infoHover').find('.ui-slider-range').css({'left': r2+'%','width':rng+'%'});
	}else{
		var rng = r2-r1;
		$(this).closest('.infoHover').find('.ui-slider-range').css({'left': r1+'%','width':rng+'%'});
	}	
	
	
});			
			
$('#applySlider').on("click", function(e){
	$('#range1Val').html($("#range1").val());
	$('#range2Val').html($("#range2").val());
	$('#winProbability').attr('checked',true);
	//$('.winProbtxt').removeClass('closed');
	$('#infodivWin').addClass('closed').removeClass('visible');
	var title = $('.infoPopup').closest('li').text();
	$('#currentSel').append('<li><span>'+title+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span>');
	$('.current').removeClass('closed');
	$('#winProbability').prop('disabled', true);
	$('#winProbability').closest('li').removeClass('shd3');
});

$('#leftcol_bob .firstleveldiv').on('click',function(e){
	if($(this).attr('id') == 'winProbability'){
		e.preventDefault();
	}else{
		$(this).closest('li').find('a.editLink').trigger('click');
	}
});

/*$('#leftcol_bob .secondlevel').on('click',function(e){
	$(this).closest('li').find('a.iceditLink').trigger('click');
});*/

$('#contentColumn').on('scroll', function() {
	$('#infodivWin').addClass('closed').removeClass('visible');
	$('.infoHover').addClass('closed').removeClass('visible');
});


$('.selview').on('click',function() {
 $('.tbldata').addClass('closed');
  $('li ul, li div.scrollContainer').addClass('closed');  
  $('div.first h3 a').html('<span class="icon"></span>' + $(this).parent('li').find('span.txtlc').text() +' Summary');
   if($(this).attr('id') =='Bob' || $(this).attr('id') =='Market' || $(this).attr('id') =='allClients') {
	$('#ibob').removeClass('closed');
  }
  else if($(this).attr('id') =='Region') {
	$('#regions').removeClass('closed');
  }
  else {
	$('#stat').removeClass('closed');
  }
	var subID = 'sub'+$(this).attr('id');
	if ($('#'+subID).length){
	   $('#'+subID+',#'+subID+' ul').removeClass('closed');
	}
	
	
});
$('.narview').on('click',function() {
	$('.subnarrow').addClass('closed');								  
	var subnarview =  'sub' + $(this).attr('id');
	$('.' +subnarview).removeClass('closed');
});


// Sprint 4 Functionality
$(document).on('click','.infoPopup', function(e){
	e.preventDefault();	
	$(this).closest('li').addClass('shd3');
	$(this).closest('li').addClass('selected').siblings().removeClass('selected');
	$('.editLink').removeClass('closed');
	var hoverPopup = $(this).attr('href');
	var linkName = $(this).closest('li').find('div').eq(0).children('input').attr('id');
	$(hoverPopup).addClass('visible').removeClass('closed');
	var boxW = $(hoverPopup).height();
	var infoHf = (boxW/2)+10;
	var topPos = ($('#'+linkName).offset().top)-infoHf;
	var leftPos = $(this).closest('li').width(); 
	$(hoverPopup).css({'top': (topPos+10)+'px','left': (leftPos+40)+'px'}).show();
	
});

$('.btncancel').on('click', function(e){
	$('.infoPopup').closest('li').removeClass('shd3');
	$('.infoHover').addClass('closed').removeClass('visible');
	
});

var cntnl = 0;
$('.checked').on('click', function(e){
	if($(this).is(':checked')){
		$('#curSelsSec').find('.selNlist').each(function(){
			cntnl++;
		});
		var rel = $(this).closest('.content').attr('rel');
		if(cntnl > 0){
			var curr = $(this).closest('.content').prev().text();
			$('.selNlist').find('#rel1'+rel).append('<li><span class="clsicn">'+curr+':'+$(this).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>'); 
		}else{
			var curr = $(this).closest('.content').prev().text();
			$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':'+$(this).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('.current').removeClass('closed');
		}
	}
});
 var ct=0;
$('.btnapply').on('click', function(e){
	$('.selected').find('input[type=checkbox]').attr('checked', false);
	$('.selected').find('.append').html('');
	var current = ($('.selected').find('input[type=checkbox]').next().text());
	$(this).closest('.infoHover').find('input:checked').each(function(e, obj){
		ct++;
		$('#instCurSel').html(ct +' Institutions meet current selections:');
		$('.selected').find('.append').append($(obj).next().text()+'<br />');
		$('#currentSel').append('<li><span>'+current+':'+$(obj).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
	});
	$('.selected').find('.append').removeClass('closed');
	$('.infoHover').addClass('closed').removeClass('visible');
	$('.infoPopup').closest('li').removeClass('shd3');
	$('.selected').find('input[type=checkbox]').attr('checked', true);
	$('.current').removeClass('closed');
	//console.log($('.selected').find('input[type=checkbox]').next().text());
});

//End Of Sprint 4 Functionality		
var wrap = $('#wrapper').width(),
	leftC = $('#firstcolumn').width(),
	rightC = wrap - leftC;

	$('#secondcolumn').css({'width':(rightC-20)+'px' });
	
	$(window).on('resize',function(){
		var wrap = $('#wrapper').width(),
		leftC = $('#firstcolumn').width(),
		rightC = wrap - leftC;
		$('#secondcolumn').css({'width':(rightC-20)+'px' });
	});

});
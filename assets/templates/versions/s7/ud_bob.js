$(document).on('bodyContLoaded', function () {
	$('.myTab').tabs();
		var doc = $(document),
			fc = $('#firstcolumn'),
			sc = $('#secondcolumn'),
			mti = $('#myTabsId'),
			mtd = $('#myTabsDiv'),
			lb = $('#leftcol_bob'),
			$wd = $('#wrapper');
		
		$('.panels .hd').off('click').on('click', function(){
			var secCW = $(window).width()-fc.outerWidth()-30;
			sc.css('width',secCW+'px');
			var winheight=$(window).height()- sc.offset().top;
			fc.css({'height':winheight+'px','overflow':'hidden'});
			if(fc.hasClass('closed') == true){
				sc.css('height',(winheight-20)+'px');
			}else{
				sc.css('height',winheight+'px');
			}
			setTimeout(function(){
				_lCol = $('#firstcolumn');
				_lCol.niceScroll({
					cursorcolor: "#c2c2c2",
					cursorborder: 'none',
					cursorwidth: '8px',
					autohidemode: false,
					zindex: 100	
				});
			},2000);
		});
		
		var winheight=$(window).height()- $('#secondcolumn').offset().top;
		$('#firstcolumn').css({'height':winheight+'px','overflow':'hidden'});
		var $fc = $('#firstcolumn').hasClass('closed');
		if($fc == true){
			$('#myTabsDiv').css('height',(winheight-20)+'px');
		}else{
			$('#myTabsDiv').css('height',winheight+'px');
		}
		var secCW = $(window).width()-$('#firstcolumn').outerWidth()-30;
		$('#secondcolumn').css('width',secCW+'px');
		var wW = $(window).width();
		var secCW = $(window).width()-$('#firstcolumn').outerWidth()-30;
		$('#secondcolumn').css('width',secCW+'px');
		$('#myTabsId').css('width',(secCW-20)+'px');
			
		$(window).resize(function(){
			var secCW = $(window).width()-$('#firstcolumn').outerWidth()-30;
			$('#secondcolumn').css('width',secCW+'px');
			var winheight=$(window).height()- $('#secondcolumn').offset().top;
			var winheight=$(window).height()- $('#secondcolumn').offset().top;
			$('#firstcolumn').css({'height':winheight+'px','overflow':'hidden'});
			var $fc = $('#firstcolumn').hasClass('closed');
			if($fc == true){
				$('#myTabsDiv').css('height',(winheight-30)+'px');
			}else{
				$('#myTabsDiv').css('height',winheight+'px');
			}
		});
		
		_lCol = $('#firstcolumn');
		_lCol.niceScroll({
				cursorcolor: "#c2c2c2",
				cursorborder: 'none',
				cursorwidth: '8px',
				autohidemode: false,
				zindex: 100
			});

		if($('html').hasClass('ie7') == true || $('html').hasClass('ie6') == true){
			$('#winProb').on('click', function(){
				//e.preventDefault();
				var linkName = $(this).closest('li').find('div').eq(0).children('input').attr('id');
				var boxW = $('#infodivWin').height();
				var infoHf = (boxW/2)+10;
				var topPos = ($('#'+linkName).offset().top)-infoHf;
				var leftPos = $(this).closest('li').width(); 
				$('#infodivWin').css({'top': (topPos+10)+'px','left': (leftPos+40)+'px'}).addClass('visible').removeClass('closed');
				return false;
				//$('#infodivWin').addClass('visible').removeClass('closed');
			});
		}

		if($('html').hasClass('ie7') == true || $('html').hasClass('ie6') == true){
			$('#oppor_link3').on('click', function(){
				//e.preventDefault();
				var curDate = new Date();
				$('#month1').val(curDate.getMonth()+1);
				$('#year1').val(curDate.getFullYear());
				var linkName = $(this).closest('li').find('div').eq(0).children('input').attr('id');
				var boxW = $('#remittanceDate').height();
				var infoHf = (boxW/2)+10;
				var topPos = ($('#'+linkName).offset().top)-infoHf;
				var leftPos = $(this).closest('li').width(); 
				$('#remittanceDate').css({'top': (topPos-30)+'px','left': (leftPos+40)+'px'}).addClass('visible').removeClass('closed');
				$('#remittanceDate').find('.pointer').css('top', '72%');
				$('.editLink').removeClass('closed');
				$('#oppor_link3').closest('li').removeClass('shd3');
				return false;
				//$('#infodivWin').addClass('visible').removeClass('closed');
			});
		}
	
		/* IE 6 & 7 popup open/close starts here */	
		if($('html').hasClass('ie7') == true || $('html').hasClass('ie6') == true){
			$(document).on('click','a[href*="#"]', function(e){
			var iePopups = $(this).attr('href');
				iePopups = '#'+iePopups.substring(iePopups.lastIndexOf('#') +1);
				$(iePopups).dialog('open');
			});
		}
		/* IE 6 & 7 popup open/close ends here */	
		
		var userInfo = store.get('user');
	   
	if(userInfo == "UWC"){
		$('#bookselect2').addClass('closed');
		$('#clientSelector').removeClass('closed');
		$('#bystate').addClass('closed');
		$('#byregion').removeClass('closed');
		//$('#myClientStates').removeClass('closed');
		//$('#clientPopupSel').removeClass('closed');
		$(document).on('click', '#state', function(){
			$('#bystate').removeClass('closed');
			$('#myClientStates').removeClass('closed');
			$('#byregion').addClass('closed');
			$('#allClientStates').addClass('closed');
		});
		
		$(document).on('click', '#region', function(){
			$('#bystate').addClass('closed');
			$('#myClientStates').addClass('closed');
			$('#byregion').removeClass('closed');
			$('#allClientStates').addClass('closed');
		});
		$(document).on('click','#radio1', function(){
			$('#mngCustLst1').addClass('closed');
			$('#mngCustLst').removeClass('closed');
		});
		$(document).on('click','#radio2', function(){
			$('#mngCustLst').addClass('closed');
			$('#mngCustLst1').removeClass('closed');
		});
		
		$(document).on('click','#saveCustomBook',function(e){
			e.preventDefault();
			if($('#radio1').is(':checked') == true){
				var bnp = $('#bookNamePopup').val(),
				bnpL = bnp.length,
				lstC = bnp.substr(0, 3),
				sdb = $('#setDfltBook').is(':checked');
				if(sdb == true){
					$('#bookselect1').append('<option value="'+bnpL+'_'+lstC+'" selected="selected" rel="'+lstC+'">'+bnp+'</option>');
					$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
				}else{
					$('#bookselect1').append('<option value="'+bnpL+'_'+lstC+'" rel="'+lstC+'">'+bnp+'</option>');
					$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
				}	
			}
			else if($('#radio2').is(':checked') == true){
				var bnp = $('#bookNamePopup').val(),
				bnpL = bnp.length,
				lstC = bnp.substr(0, 3),
				sdb = $('#setDfltBook').is(':checked');
				if(sdb == true){
					$('#allClients').append('<option value="'+bnpL+'_'+lstC+'" selected="selected" rel="'+lstC+'">'+bnp+'</option>');
					$('#mngCustLst1').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
				}else{
					$('#allClients').append('<option value="'+bnpL+'_'+lstC+'" rel="'+lstC+'">'+bnp+'</option>');
					$('#mngCustLst1').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
				}
			}else{
			
			}
			if($('#bookNamePopup').val() == ''){
				e.preventDefault();
				$('#alertModule1').addClass('visible').removeClass('hidden');
			}else{
				$('#createBookPopup').dialog('close');
			}
			var creatbook = $('#curSelPopup').html();
			$('#curSelsSec').append('<ul id="'+lstC+'" class="nlist">'+creatbook+'</ul>');
			$('#bookNamePopup').val('');
		});
		
		$(document).on('click','#applyMngCust',function(e){
			var renBk = $('#renameBook').val(),
				copyBk = $('#copyBook').val(),
				radClt = $('#radio1').is(':checked'),
				actSel = $('#actionSelect').find(":selected").text();
			if(actSel == "Rename" && $('#radio1').is(':checked') == true ){
				if(renBk != ''){
					$('#mngCustLst').find('.shdhlt').html('<span>'+renBk+'</span>');
					var renItem = $('#mngCustLst').find('.shdhlt').attr('rel');
					$('#bookselect1').find('option[rel='+renItem+']').text(renBk);
					$('#manageBookPopup').dialog('close');
				}else{
					$('#alertModule').addClass('visible').removeClass('hidden');
				}
			}else if(actSel == "Rename" && $('#radio2').is(':checked') == true ){
				if(renBk != ''){
					$('#mngCustLst1').find('.shdhlt').html('<span>'+renBk+'</span>');
					var renItem = $('#mngCustLst1').find('.shdhlt').attr('rel');
					$('#allClients').find('option[rel='+renItem+']').text(renBk);
					$('#manageBookPopup').dialog('close');
				}else{
					$('#alertModule').addClass('visible').removeClass('hidden');
				}
			}else if(actSel == "Make Copy"){
				if($('#radio1').is(':checked') == true){
					$('#mngCustLst').find('.shdhlt').html('<span>'+copyBk+'</span>');
						var copyItem = $('#mngCustLst').find('.shdhlt').attr('rel');
						$('#bookselect1').find('option[rel='+copyItem+']').text(copyBk);
						$('#copyBook').val('');
						$('#manageBookPopup').dialog('close');
				}else{
					$('#mngCustLst1').find('.shdhlt').html('<span>'+copyBk+'</span>');
					var copyItem = $('#mngCustLst1').find('.shdhlt').attr('rel');
					$('#allClients').find('option[rel='+copyItem+']').text(copyBk);
					$('#copyBook').val('');
					$('#manageBookPopup').dialog('close');
				}
			}else if(actSel == "Set As Default Book"){
				if($('#radio1').is(':checked') == true){
					var mcl = $('#mngCustLst').find('.shdhlt').text();
						$('#bookselect1').append('<option selected="selected">'+mcl+'</option>');
						$('#manageBookPopup').dialog('close');
				}else{
					var mcl = $('#mngCustLst1').find('.shdhlt').text();
						$('#allClients').append('<option selected="selected">'+mcl+'</option>');
						$('#manageBookPopup').dialog('close');
				}
			$('#manageBookPopup').dialog('close');
			}else if(actSel == "Delete"){
				$('#bnam').html('');
				var delItem = $('#mngCustLst').find('.shdhlt').html();
				var delItem1 = $('#mngCustLst1').find('.shdhlt').html();
				if($('#radio1').is(':checked') == true){
					$('#bnam').append(' '+delItem);
					$('#bnam').removeClass('closed');
					$('#deleteBook').dialog('open');
				}else{
					$('#bnam').append(' '+delItem1);
					$('#bnam').removeClass('closed');
					$('#deleteBook').dialog('open');
				}
			}else{
			}
			//$('#manageBookPopup').dialog('close');
		});
		$(document).on('click', '#delBook', function(){
			if($('#radio1').is(':checked') == true){
					var delItem = $('#mngCustLst').find('.shdhlt').attr('rel');
					$('#mngCustLst').find('.shdhlt').addClass('closed');
					$('#bookselect1').find('option[rel='+delItem+']').remove();
					$('#deleteBook').dialog('close');
					$('#manageBookPopup').dialog('close');
					
			}else if($('#radio2').is(':checked') == true){
				var delItem = $('#mngCustLst').find('.shdhlt').attr('rel');
				$('#mngCustLst1').find('.shdhlt').addClass('closed');
				$('#allClients').find('option[rel='+delItem+']').remove();
				$('#deleteBook').dialog('close');
			}else{
				$('#deleteBook').dialog('close');
			}
		});
	}else{
		$('#clientSelector').addClass('closed');
		$('#bookselect1').addClass('closed');
		$('#byregion').removeClass('closed');
		//$('#allClientStates').removeClass('closed');
		//$('#myClientStates').addClass('closed');
		$('#clientPopupSel').addClass('closed');
		$(document).on('click', '#region', function(){
			$('#bystate').addClass('closed');
			$('#myClientStates').addClass('closed');
			$('#byregion').removeClass('closed');
			$('#allClientStates').addClass('closed');
		});
		$(document).on('click', '#state', function(){
			$('#bystate').removeClass('closed');
			$('#myClientStates').addClass('closed');
			$('#byregion').addClass('closed');
			$('#allClientStates').removeClass('closed');
		});
		$(document).on('click','#saveCustomBook',function(e){
			var bnp = $('#bookNamePopup').val(),
			bnpL = bnp.length,
			lstC = bnp.substr(0, 3),
			sdb = $('#setDfltBook').is(':checked');
			if(sdb == true){
				$('#bookselect2').append('<option value="'+bnpL+'_'+lstC+'" selected="selected" rel="'+lstC+'">'+bnp+'</option>');
				$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
			}else{
				$('#bookselect2').append('<option value="'+bnpL+'_'+lstC+'" rel="'+lstC+'">'+bnp+'</option>');
				$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
			}
			if($('#bookNamePopup').val() == ''){
				e.preventDefault();
				$('#alertModule1').addClass('visible').removeClass('hidden');
			}else{
				$('#alertModule1').addClass('hidden').removeClass('visible');
				$('#createBookPopup').dialog('close');
			}
			$('#bookNamePopup').keyup(function(){
				if($(this).val()!=""){
					$('#alertModule1').addClass('hidden').removeClass('visible');
				}
			});
			var creatbook = $('#curSelPopup').html();
				$('#curSelsSec').append('<ul id="'+lstC+'" class="nlist">'+creatbook+'</ul>');
				$('#bookNamePopup').val('');

		});
			$(document).on('click','#applyMngCust',function(e){
			var renBk = $('#renameBook').val(),
			actSel = $('#actionSelect').find(":selected").text();
		if(actSel == "Rename"){
			if(renBk != ''){
				$('#alertModule').addClass('hidden').removeClass('visible');
				$('#mngCustLst').find('.shdhlt').html('<span>'+renBk+'</span>');
				var renItem = $('#mngCustLst').find('.shdhlt').attr('rel');
				$('#bookselect2').find('option[rel='+renItem+']').text(renBk);
				$('#renameBook').val('');
				$('#manageBookPopup').dialog('close');
			}else{
				$('#alertModule').addClass('visible').removeClass('hidden');
			}
		}else if(actSel == "Set As Default Book"){
			var mcl = $('#mngCustLst').find('.shdhlt').text();
				$('#bookselect2').append('<option selected="selected">'+mcl+'</option>');
				$('#manageBookPopup').dialog('close');
		}else if(actSel == "Delete"){
			$('#bnam').html('');
			var delItem = $('#mngCustLst').find('.shdhlt').html();
			$('#bnam').append(' '+delItem);
			$('#bnam').removeClass('closed');
			$('#deleteBook').dialog('open');
		}else{	}
	});
	$(document).on('click', '#delBook', function(){
		var delItem = $('#mngCustLst').find('.shdhlt').attr('rel');
		$('#mngCustLst').find('.shdhlt').addClass('closed');
		$('#bookselect2').find('option[rel='+delItem+']').remove();
		$('#deleteBook').dialog('close');
		$('#manageBookPopup').dialog('close');
	});
	}

	$(document).on('click','#bookselect1', function(e){
		$('#radio1').attr('checked',true);
		
	});
	$(document).on('click','#allClients', function(e){
		$('#radio2').attr('checked',true);
	});
	$(document).on('click','#radio2', function(e){
		$('#allClientStates').removeClass('closed');
		$('#myClientStates').addClass('closed');
		$('#allClients').removeClass('closed');
		$('#bookselect1').addClass('closed');
	});
		
	$(document).on('click','#radio1', function(e){
		$('#allClientStates').addClass('closed');
		$('#myClientStates').removeClass('closed');
		$('#allClients').addClass('closed');
		$('#bookselect1').removeClass('closed');
		
	});
	$(document).on('click','#radio1, #radio2', function(e){
		if($('.current').hasClass('closed')== false){
			$('#alertPopup').dialog('open');
		}
		if($('#curSelsSec').hasClass('closed')== false){
			$('#alertPopup').dialog('open');
		}
	});
	$(document).on('click','#cancelViewChange', function(e){
		if($('#radio1').is(':checked') == true){
			$('#radio2').attr('checked',true);
			//$('#radio1').attr('checked',false);
		}
		if($('#radio2').is(':checked') == true){
			$('#radio1').attr('checked',true);
			//$('#radio2').attr('checked',false);
		}
	});
	$(document).on('click','#viewChange', function(e){
		if($('#radio1').is(':checked') == true){
			$('#radio1').attr('checked',true);
			$('.curSels').removeClass('closed');
			$('#curSelsSec').addClass('closed');
			$('.current').addClass('closed');
			$('#instCurSel').html('None');
		}
		if($('#radio2').is(':checked') == true){
			$('#radio2').attr('checked',true);
			$('.curSels').removeClass('closed');
			$('#curSelsSec').addClass('closed');
			$('.current').addClass('closed');
			$('#instCurSel').html('None');
		}
	});
	
	$(document).on('change','#bookselect1 , #bookselect2 , #allClients', function(e){
		var bnpV = $(this).val(),
			bnp = bnpV.split('_');
		$('#curSelsSec').removeClass('closed');
		$('.selBook').html($(this).find(":selected").text());
		$('.curSels').html();
		$('#instCurSel').html(bnp[0]+' Institutions meet current selections:');
		$('#'+bnp[1]).addClass('selNlist').removeClass('closed').siblings().addClass('closed').removeClass('selNlist');
		$('.current').addClass('closed');
		$('.defaultOpt').attr('selected', 'selected');
	});
	
	$('#curSelsSec').on('click','.icdeleteIcon',function(e){
		$(this).closest('li').addClass('closed');
	});
	
	$(document).on('click','#clearAllRef',function(e){
		$('#currentSel').find('ul').html('');
		$('.curSels').removeClass('closed');
		$('#curSelsSec').addClass('closed');
		$('.current').addClass('closed');
		$('#instCurSel').html('None');
	});

	$(document).on('change','#actionSelect', function(e){
		var copyBk = $('#mngCustLst').find('.shdhlt').text(),
			copyBk1 = $('#mngCustLst1').find('.shdhlt').text(),
			renBk = $('#mngCustLst').find('.shdhlt').text(),
			renBk1 = $('#mngCustLst1').find('.shdhlt').text();
		if($(this).find(":selected").text() == "Rename" && $('#radio1').is(':checked')== true){
			$('#renameBook').removeClass('closed');
			$('#renameBook').val(renBk);
		}else if($(this).find(":selected").text() == "Rename" && $('#radio2').is(':checked')== true){
			$('#renameBook').removeClass('closed');
			$('#renameBook').val(renBk1);
		}else{
			$('#renameBook').addClass('closed');
		}
		if($(this).find(":selected").text() == "Make Copy" && $('#radio1').is(':checked')== true){
			$('#copyBook').removeClass('closed');
			$('#copyBook').val('Copy of'+' '+copyBk);
		}else if($(this).find(":selected").text() == "Make Copy" && $('#radio2').is(':checked')== true){
			$('#copyBook').removeClass('closed');
			$('#copyBook').val('Copy of'+' '+copyBk1);
		}else{
			$('#copyBook').addClass('closed');
		}
	});
	
	$(document).on('click','#createBook', function(e){
		$('#curSelPopup').html('');
		var creatbook = $('#currentSel').html();
		//console.log(creatbook);
		$('#curSelPopup').append(creatbook);
	});

	$(document).on('click','#mngCustLst li',function(e){
		$(this).addClass('shdhlt').siblings().removeClass('shdhlt');
		$('#actionSelect').removeAttr('disabled');
	});
	
	$(document).on('click','#mngCustLst1 li',function(e){
		$(this).addClass('shdhlt').siblings().removeClass('shdhlt');
		$('#actionSelect').removeAttr('disabled');
	});
	
	$(document).on('click','.icdeleteIcon',function(e){
		$(this).closest('li').addClass('closed');
	});
	
	$('.hd').find('h3').on('click', function(e){
		e.preventDefault();
		if($(this).closest('div').hasClass('hdexpanded') == true){
			$(this).find('a').addClass('collapsed').removeClass('expanded');
			$(this).closest('div').next().addClass('expanded bgexpanded');
		}else{
			$(this).closest('div').next().addClass('expanded bgexpanded').siblings('.content').removeClass('expanded bgexpanded').hide();
			$(this).closest('div').next().siblings('.hd').removeClass('hdexpanded').find('a').addClass('collapsed').removeClass('expanded');
		}
	});
	
	$('#leftcol_bob .secondlevel').on('click',function(e){
		if($(this).attr('checked')){
			$(this).closest('ul').parent().addClass('activeli');
			$(this).closest('content').parent().addClass('activeli');
		}
		else{
			$(this).closest('ul').parent().removeClass('activeli');	
		}
	});
	
	$(document).on('click','#channel, #lastVisited, #status', function(e){
		$('#ui-id-2').trigger('click');
		$('#tbl_ibob_interactions thead').find('th').eq(4).addClass('headerSortDown').siblings().removeClass('headerSortDown headerSortUp');
	});
	
	$(document).on('click','#opportunityType, #serviceModel, #salesPhase, #winProbability, #productAndServices', function(e){
		$('#ui-id-3').trigger('click');
	});
	
//Select all function
$(document).on('click','#selectAll', function(){
	if($(this).is(':checked')){
		$(this).closest('ul').next().find('input[type=checkbox]').prop('checked', true);
		$(this).closest('ul').next().find('input[type=checkbox]').trigger('click');
		$(this).closest('ul').next().find('input[type=checkbox]').prop('checked', true);
		$('#selectAllOpen').find('.content1').addClass('closed');
		$('#selectAllOpen').find('.clearspan').addClass('closed');
		$('#selectAllOpen').find('.editLink').removeClass('closed');
		$('#selectAllOpen').find('.firstlevel').prop('disabled', true);
	}else{
		$(this).closest('ul').next().find('input[type=checkbox]').prop('checked', false);
	}
});
//Select all function ends
//Parent Checkbox Selects All
$(document).on('change','.firstlevel', function(e){
	if($(this).is(':checked')){
		$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').prop('checked', true);
		$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').trigger('click');
		$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').prop('checked', true);
		$('.secondlevel').closest('ul').parent().addClass('activeli');
	}
	else{
		$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').prop('checked', false);
		$('.secondlevel').closest('ul').parent().removeClass('activeli');
	}
	//$(this).prop('checked', false);
});

var ct = 0;
$('.secondleveldiv').find('input[type=checkbox]').on('click', function(){
	if($(this).is(":checked") == false){
		$(this).closest('.secondleveldiv').parent('li').find('.firstlevel').prop('indeterminate', true);
	}
});
/*$('.secondleveldiv').find('input[type=checkbox]').on('click', function(){
	if($(this).is(":checked") == true){
		$(this).closest('.secondleveldiv').parent('li').find('.firstlevel').prop('indeterminate', true);
	}
	else{
		$(this).closest('.secondleveldiv').parent('li').find('.firstlevel').prop('indeterminate', true);
	}
	$(this).closest('.secondleveldiv').parent('li').find('.firstlevel').prop('checked', false);
});*/
//First Level Functionality
$('#leftcol_bob a.editLink').on('click',function(e){
	$(this).closest('li').find('.secondleveldiv').addClass('closed');
	$(this).closest('li').find('.secondleveldiv').removeClass('closed');
	$(this).closest('li').siblings().addClass('shd3').find('.content1').addClass('closed');
	$(this).closest('.content').find('li.shd3').each(function( index ) {	
		$(this).removeClass('shd3');
		$(this).find('.clearspan').addClass('closed');
		$(this).find('.editLink').removeClass('closed');
		$(this).find('.secondleveldiv li').each(function(index) {		
			if($(this).hasClass('activeli') && $(this).parent().hasClass('nlist') ){		
				$(this).removeClass('closed');	
			}else{
				if($(this).parent().hasClass('nlist')){
					$(this).addClass('closed');
				}
			}
		});
	});
		$(this).closest('li').find('.clearspan').removeClass('closed');	
		$(this).closest('li').addClass('shd3');
		$(this).closest('li').find('.secondleveldiv').removeClass('closed');
		$(this).closest('li').find('.secondleveldiv').find('.secondlevel').removeClass('hidden');
		$(this).addClass('closed');
		$(this).closest('li').find('.secondleveldiv li').each(function( index ) {	
			$(this).parent().parent().removeClass('closed');
			if( $(this).parent().hasClass('nlist')){
			 $(this).find('input[type=checkbox]').removeClass('closed');
			 $(this).removeClass('closed');
			}
		});
		$(this).closest('li').find('.firstleveldiv').find('input[type=checkbox]').prop('disabled', false);
		$(this).closest('li').find('.secondleveldiv').find('.teamScroll').removeClass('closed');	
});

$('#leftcol_bob .icclearLink').on('click',function(e){
	$(this).parent().parent().removeClass('shd3');
	$(this).parent().parent().find('.editLink').removeClass('closed');
	$(this).parent().addClass('closed');
	
	$(this).parent().parent().find('.secondleveldiv').addClass('closed');
	var ct = 0;
	$(this).parent().parent().find('.secondleveldiv li').each(function() {	
		var chkbox = $(this).find('input[type=checkbox]').is(':checked'),
			radBtn = $(this).find('input[type=radio]').is(':checked');
		if(chkbox == true || radBtn == true){
			ct++;
		}
	});
	$(this).closest('li').find('.firstleveldiv').find('input[type=checkbox]').prop('disabled', true);
	$(this).closest('li').find('.secondleveldiv').find('.teamScroll').addClass('closed');
});

$('.rad').on('click', function(e){
	$('.sel').remove();
	$(this).closest('ul').parent().addClass('activeli').siblings().removeClass('activeli');
	if($(this).closest('ul').parent().hasClass('activeli')== true){
		var radio = $(this).next().text();
	}
	var curr = ($(this).closest('.content').prev().text());
	$('#currentSel').append('<li class="sel"><span class="clsicn">'+curr+':<span class="mls"></span>'+ radio+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
	$('.current').removeClass('closed');
});

$('#winCancel').on("click", function(e){
	$('#infodivWin').addClass('closed').removeClass('visible');
});
 $('#assetCancel').on("click", function(e){
	$('#assetClassId').addClass('closed').removeClass('visible');
});

$( "#timeframe-slider" ).slider({
	orientation: "horizontal",
	range: true,
	value:100,
	min: 0,
	max: 100,
	step: 1,
	values: [ 10,30 ],
	slide: function( event, ui ) {
		$( "#range1" ).val( ui.values[ 0 ]);
		$( "#range2" ).val( ui.values[ 1 ]);
	}
});
	//$('#range1').val($('#range1Val').text());
	//$('#range2').val($('#range2Val').text());
	
$(document).on('keyup','#range1, #range2', function(){
	var $ran1 = $(this).val(),
	num = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/,
	slider = $(this).closest('.infoHover').find('.slider');
	if(!num.test($ran1) || $ran1 > 100) {	
		$(this).addClass('descl');
	}else{
		$(this).removeClass('descl');
		if($(this).attr('id') == 'range1'){
			slider.find('a').eq(0).css({'left': $ran1+'%'});
		}else{
			slider.find('a').eq(1).css({'left': $ran1+'%'});
		}
	}
	var r1 = $('#range1').val(),
		r2 = $('#range2').val();
	if(r1 > r2){
		var rng = r1-r2;
		$(this).closest('.infoHover').find('.ui-slider-range').css({'left': r2+'%','width':rng+'%'});
	}else{
		var rng = r2-r1;
		$(this).closest('.infoHover').find('.ui-slider-range').css({'left': r1+'%','width':rng+'%'});
	}
});			
	
$('#applySlider').on("click", function(e){
	$('#range1Val').html($("#range1").val());
	$('#range2Val').html($("#range2").val());
	$('#winProbability').attr('checked',true);
	//$('.winProbtxt').removeClass('closed');
	$('#infodivWin').addClass('closed').removeClass('visible');
	var title = $('.infoPopup').closest('li').text();
	$('#currentSel').append('<li><span>'+title+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span>');
	$('.current').removeClass('closed');
	$('#winProbability').prop('disabled', true);
	$('#winProbability').closest('li').removeClass('shd3');
});

$('#leftcol_bob .firstleveldiv').on('click',function(e){
	if($(this).attr('id') == 'winProbability'){
		e.preventDefault();
	}else{
		$(this).closest('li').find('a.editLink').trigger('click');
	}
});

/*$('#leftcol_bob .secondlevel').on('click',function(e){
	$(this).closest('li').find('a.iceditLink').trigger('click');
});*/

$('#firstcolumn').on('scroll', function() {
	$('#infodivWin').addClass('closed').removeClass('visible');
	$('.infoHover').addClass('closed').removeClass('visible');
	$('#assetClassId').addClass('closed').removeClass('visible');
	$('#remittanceDate').addClass('closed').removeClass('visible');
});

$('.selview').on('click',function() {
	$('.tbldata').addClass('closed');
	$('li ul, li div.scrollContainer').addClass('closed');  
	$('div.first h3 a').html('<span class="icon"></span>' + $(this).parent('li').find('span.txtlc').text() +' Summary');
	if($(this).attr('id') =='Bob' || $(this).attr('id') =='Market' || $(this).attr('id') =='allClients') {
		$('#ibob').removeClass('closed');
	}else if($(this).attr('id') =='Region') {
		$('#regions').removeClass('closed');
	}else{
		$('#stat').removeClass('closed');
	}
	var subID = 'sub'+$(this).attr('id');
	if ($('#'+subID).length){
		$('#'+subID+',#'+subID+' ul').removeClass('closed');
	}	
});

$('.narview').on('click',function() {
	$('.subnarrow').addClass('closed');								  
	var subnarview =  'sub' + $(this).attr('id');
	$('.' +subnarview).removeClass('closed');
});

// Sprint 4 Functionality
$(document).on('click','.infoPopup', function(e){
	e.preventDefault();	
		$(this).closest('li').addClass('shd3');
		$(this).closest('li').addClass('selected').siblings().removeClass('selected');
		$('.editLink').removeClass('closed');
		var hoverPopup = $(this).attr('href');
		var linkName = $(this).closest('li').find('div').eq(0).children('input').attr('id');
		$(hoverPopup).addClass('visible').removeClass('closed');
		var boxW = $(hoverPopup).height();
		var infoHf = (boxW/2)+10;
		var topPos = ($('#'+linkName).offset().top)-infoHf;
		var leftPos = $(this).closest('li').width(); 
		$(hoverPopup).css({'top': (topPos+10)+'px','left': (leftPos+40)+'px'}).show();
	
});

$(document).on('click','.infoPopup1', function(e){
	e.preventDefault();	
	$(this).closest('li').addClass('shd3');
	$(this).closest('li').addClass('selected').siblings().removeClass('selected');
	$('.editLink').removeClass('closed');
	var hoverPopup = $(this).attr('href');
	var linkName = $(this).closest('li').find('div').eq(0).children('input').attr('id');
	$(hoverPopup).addClass('visible').removeClass('closed');
	var boxW = $(hoverPopup).height();
	var infoHf = (boxW/2)+10;
	var topPos = ($('#'+linkName).offset().top)-infoHf;
	var leftPos = $(this).closest('li').width(); 
	$(hoverPopup).css({'top': (topPos-45)+'px','left': (leftPos+40)+'px'}).show();
	$('#remittanceDate').find('.pointer').css('top','75%');
});

function showInfoHover(objl, objdiv, pos) {
	var objl = $(objl);
    var thisPos = objl.closest('li').offset();
    var leftPos = thisPos.left + objl.closest('li').width() + 20;  // px for padding, border
    var infoHoverHeight = $(objdiv).height();
    var topPos = thisPos.top-($(objdiv).height()/2); // px for dropdown icon width
    var wH = $(window).height()-200;
    var lessVal = thisPos.top-wH,
        pntrOT = ($(objdiv).height()/2)+lessVal;
    if(wH < thisPos.top){
        $(objdiv).css('top', (topPos-lessVal) + 'px').css('left', (leftPos) + 'px').show();
        $(objdiv).find('.pointer').css('top', pntrOT+'px');
    }else{
        $(objdiv).css('top', (topPos) + 'px').css('left', (leftPos) + 'px').show();
    }
}
$("body").on("click", "#oppor_link2", function(){
	//e.stopPropagation();
	//e.preventDefault();	
	$('#investmentId').addClass('visible').removeClass('hidden');
	showInfoHover(this,'#investmentId');
	$('.editLink').removeClass('closed');
	$('#oppor_link2').closest('li').removeClass('shd3');
	$('#assetClassId').addClass('hidden');		 
	$('#remittanceDate').addClass('hidden');
	$('#transferVendor').addClass('hidden');
	return false;	
});
$("body").on("click", "#oppor_link1", function(){
	$('#assetClassId').addClass('visible').removeClass('hidden');
	showInfoHover(this,'#assetClassId');
	$('.editLink').removeClass('closed');
	$('#oppor_link1').closest('li').removeClass('shd3');
	$('#investmentId').addClass('hidden');
	$('#remittanceDate').addClass('hidden');
	$('#transferVendor').addClass('hidden');
	return false;
});
$("body").on("click", "#oppor_link4", function(){
	$('#transferVendor').addClass('visible').removeClass('hidden');
	showInfoHover(this,'#transferVendor');
	$('.editLink').removeClass('closed');
	$('#oppor_link4').closest('li').removeClass('shd3');
	$('#investmentId').addClass('hidden');
	$('#remittanceDate').addClass('hidden');
	$('#assetClassId').addClass('hidden');
	return false;
});
$("#oppor_link3").on("click", function(e){
	//e.stopPropagation();
	e.preventDefault();	
	var curDate = new Date(),
		curM = curDate.getMonth()+1,
		curY = curDate.getFullYear(),
		preY = curY - 1;
		$('#year').find('option:eq(0)').val(preY).text(preY);
		$('#year').find('option:eq(1)').val(curY).text(curY);
		$('#year1').find('option:eq(0)').val(preY).text(preY);
		$('#year1').find('option:eq(1)').val(curY).text(curY);
		$('#month1').val(curM);
		$('#month').val(curM);
		$('#month1').find('option').removeAttr('disabled');
		$('#month1').find('option').each(function(){
			if($(this).val() > curM){
				$(this).attr('disabled',true);
			}
		});
		$('#month').find('option').each(function(){
			if($(this).val() < curM){
				$(this).attr('disabled',true);
			}
		});
	$('#remittanceDate').addClass('visible').removeClass('hidden');
	showInfoHover(this,'#remittanceDate');
	$('.editLink').removeClass('closed');
	$('#oppor_link3').closest('li').removeClass('shd3');
	$('#investmentId').addClass('hidden');
	$('#assetClassId').addClass('hidden');
	$('#transferVendor').addClass('hidden');
});
 
$('.btncancel').on('click', function(e){
	$('.infoPopup').closest('li').removeClass('shd3');
	$('.infoHover').addClass('closed').removeClass('visible');
	$('.infoPopup1').closest('li').removeClass('shd3');
	$('#assetClass1').prop('disabled', true);
	$('#date').prop('disabled', true);
});

var cntnl = 0;
$('.checked').on('click', function(e){
	if($(this).is(':checked')){
		$('#curSelsSec').find('.selNlist').each(function(){
			cntnl++;
		});
		var rel = $(this).closest('.content').attr('rel');
		if(cntnl > 0){
			var curr = $(this).closest('.content').prev().text();
			$('.selNlist').find('#rel1'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(this).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>'); 
		}else{
			var curr = $(this).closest('.content').prev().text();
			$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(this).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('.current').removeClass('closed');
		}
	}
});

var ct=0;
$('.btnapply').on('click', function(e){
	$('.selected').find('input[type=checkbox]').attr('checked', false);
	$('.selected').find('.append').html('');
	var current = ($('.selected').find('input[type=checkbox]').next().text());
	$(this).closest('.infoHover').find('input:checked').each(function(e, obj){
		ct++;
		$('#instCurSel').html(ct +' Institutions meet current selections:');
		$('.selected').find('.append').append($(obj).next().text()+'<br />');
		$('#currentSel').append('<li><span>'+current+':'+$(obj).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
	});
	$('.selected').find('.append').removeClass('closed');
	$('.infoHover').addClass('closed').removeClass('visible');
	$('.infoPopup').closest('li').removeClass('shd3');
	$('.selected').find('input[type=checkbox]').attr('checked', true);
	$('.current').removeClass('closed');
});

//End Of Sprint 4 Functionality	
function setTableContWidth(){	
	var leftcontwid=210;
	if($('#contentColumn').width()<=740){		
		if($('#firstcolumn').hasClass('closed')){
			var secondColWid= $('#wrapper').width();		
		}else{
			var secondColWid=740;
		}		
	}else{		
		if($('#firstcolumn').hasClass('closed')){
			var secondColWid= $('#wrapper').width();
		}else{
			var secondColWid= $(window).width()-$('#firstcolumn').outerWidth();
		}	
	}
}
	
// Sprint 6 Functionality
$('#oppor_link1').on('click',function(e){
	e.preventDefault();
	$('#assetClassId').removeClass('closed').addClass('visible');
});
$('#oppor_link3').on('click',function(e){
	e.preventDefault();
	$('#remittanceDate').removeClass('closed').addClass('visible');
});
$('#oppor_link4').on('click',function(e){
	e.preventDefault();
	$('#transferVendor').removeClass('closed').addClass('visible');
});
$('#oppor_link2').on('click',function(e){
	e.preventDefault();
	$('#investmentId').removeClass('closed').addClass('visible');
});

$(document).on('click','.allSel',function(){
	$(this).closest('ul').find('.sel').attr('checked', this.checked);
});

$(document).on('click','.sel',function(){
	if($(this).closest('div').find('.sel').length == $(this).closest('div').find('.sel:checked').length) {
		$(this).closest('div').find('.allSel').attr("checked", "checked");
	} else {
		$(this).closest('div').find('.allSel').removeAttr("checked");
	}
});

$('#assetapply').on("click", function(e){
	//$('#currentSel').html('');
	var curr = $('#assetClass1').next().text();
	var rel = $('#oppor_link1').closest('.content').attr('rel');
	if($('.allSel:checked').length > 0) {
		$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(".allSel").next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('.current').removeClass('closed');
	}else{
		$('.sel:checked').each(function(){
			$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(this).next().text()+'</span><span class="mls"></span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('.current').removeClass('closed');
		});
	}
	if($('input[name=ac]:checked').length > 0){
		var asof = $('#testinput-Date-picker1').val();
		$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$("input[name=ac]:checked").next().text()+' '+asof+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('.current').removeClass('closed');
	}
	$('#assetClass1').prop('checked', true);
	$('#assetClass1').prop('disabled', true);
	$('.infoHover').addClass('closed').removeClass('visible');
});	

$('#transfersApply').on("click", function(e){
	var curr = $('#transfersFil').next().text();
	var rel = $('#oppor_link4').closest('.content').attr('rel');
	if($('.allSel:checked').length > 0) {
		$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(".allSel").next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('.current').removeClass('closed');
	}else{
		$('.sel:checked').each(function(){
			$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(this).next().text()+'</span><span class="mls"></span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('.current').removeClass('closed');
		});
	}
	if($('input[name=ac]:checked').length > 0){
		var asof = $('#testinput-Date-picker1').val();
		$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$("input[name=ac]:checked").next().text()+' '+asof+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('.current').removeClass('closed');
	}
	$('#transfersFil').prop('checked', true);
	$('#transfersFil').prop('disabled', true);
	$('.infoHover').addClass('closed').removeClass('visible');
});	

$('#btnapply').on("click", function(e){
	var curr = $('#investments').next().text();
	var rel = $('#oppor_link2').closest('.content').attr('rel');
	if($('.fcompany:checked').length > 0) {
		$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(".fcompany").next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('.current').removeClass('closed');
	}else if($('.iType:checked').length > 0) {
		$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(".iType").next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('.current').removeClass('closed');
	}else{
		$('.sels:checked').each(function(){
			$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(this).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('.current').removeClass('closed');
		});
	}
	$('#investments').prop('checked', true);
	$('#investments').prop('disabled', true);
	$('#investments').closest('li').removeClass('shd3');
});

$(document).on("change",'#year', function(e){
	var curDate = new Date(),
		curM = curDate.getMonth()+1,
		curY = curDate.getFullYear(),
		preY = curY - 1;
	if($(this).val() == preY){
		$('#month').val(curM);
		$('#month').find('option').removeAttr('disabled');
		$('#month').find('option').each(function(){
				if($(this).val() < curM){
					$(this).attr('disabled',true);
				}
			});
	}else{
		$('#month').val(curM);
		$('#month').find('option').removeAttr('disabled');
		$('#month').find('option').each(function(){
			if($(this).val() > curM){
				$(this).attr('disabled',true);
			}
		});
	}
});

$(document).on("change",'#year1', function(e){
	var curDate = new Date(),
		curM = curDate.getMonth()+1,
		curY = curDate.getFullYear(),
		preY = curY - 1;
	if($(this).val() == preY){
		$('#month1').val(curM);
		$('#month1').find('option').removeAttr('disabled');
		$('#month1').find('option').each(function(){
				if($(this).val() < curM){
					$(this).attr('disabled',true);
				}
			});
	}else{
		$('#month1').val(curM);
		$('#month1').find('option').removeAttr('disabled');
		$('#month1').find('option').each(function(){
			if($(this).val() > curM){
				$(this).attr('disabled',true);
			}
		});
	}
});

$(document).on("click",'#applyDate', function(e){
	$('.Otd').remove();
	var month = $('#month').find('option:selected').text(),
		month_val = $('#month').val(),
		year = $('#year').find('option:selected').text(),
		year_val = $('#year').val(),
		month1 = $('#month1').find('option:selected').text(),
		month1_val = $('#month1').val(),
		year1 = $('#year1').find('option:selected').text(),
		year1_val = $('#year1').val(),
		curr = $('#oppor_link3').closest('.selected').find('label').text(),
		rel = $('#oppor_link3').closest('.content').attr('rel'),
		res_year = year1_val- year_val,
		res_month = month1_val- month_val;
	if(res_month>= 1 && res_year <= 1 ){
		res_year = res_year + 1;
	}else{}
	if( res_year < 2){
		if(month_val < month1_val && year_val <= year1_val){
			$('#currentSel').find('#rel'+rel).append('<li class="Otd"><span class="clsicn">'+curr+':<span class="mls"></span>'+month+' '+year+' to<br>'+month1+' '+year1+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('#dateRange').addClass('hidden');
			$('.current').removeClass('closed');
			$('.infoHover').addClass('closed').removeClass('visible');
			$('#date').prop('checked', true);
			$('#date').prop('disabled', true);
		}else if(year_val < year1_val && month_val >= month1_val){
			$('#currentSel').find('#rel'+rel).append('<li class="Otd"><span class="clsicn">'+curr+':<span class="mls"></span>'+month+' '+year+' to<br>'+month1+' '+year1+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('#dateRange').addClass('hidden');
			$('.current').removeClass('closed');
			$('.infoHover').addClass('closed').removeClass('visible');
			$('#date').prop('checked', true);
			$('#date').prop('disabled', true);
		}else{
			$('#dateRange').removeClass('hidden');
		}
	}else{
		$('#dateRange').removeClass('hidden');
	}
});

$(document).on('click','#cancelDate', function(e){
	$('#dateRange').addClass('hidden');
});

$(document).on('click','#fundComp', function(e){
	$('#FundCompany').removeClass('closed');
	$('#InvestType').addClass('closed');
});
$(document).on('click','#investType', function(e){
	$('#InvestType').removeClass('closed');
	$('#FundCompany').addClass('closed');
});

/*$('.temp').on('click', function(e){
	e.stopImmediatePropagation();
	if($(this).closest('.scrollCont1').find('.sels').length == $(this).closest('.scrollCont1').find('.sels:checked').length) {
		$(this).closest('.scrollCont1').find('.allSels').attr("checked", "checked");
	} else {
		$(this).closest('.scrollCont1').find('.allSels').removeAttr("checked");
	}
});*/

$('.thirdlev').on('change', function(){
	//e.stopImmediatePropagation();
	if($(this).closest('ul').find('input[type=checkbox]').length == $(this).closest('ul').find('input:checked').length) {
		$(this).closest('.content').prev().find('.firstlev').attr('checked', true)
	} else {
		$(this).closest('.content').prev().find('.firstlev').attr('checked', false);
	}
});

$('.sublev').on('change',function(){
	if($(this).closest('.content').find('input[type=checkbox]').length == $(this).closest('.content').find('input:checked').length) {
		$(this).closest('.content').prev().find('.firstlev').attr('checked', true)
	} else {
		$(this).closest('.content').prev().find('.firstlev').attr('checked', false);
	}
});

$('.sublev1').on('change',function(){
	if($(this).closest('.content').find('input[type=checkbox]').length == $(this).closest('.content').find('input:checked').length) {
		$(this).closest('.content').prev().find('.firstlev').attr('checked', true)
	} else {
		$(this).closest('.content').prev().find('.firstlev').attr('checked', false);
	}
});

$(document).on('click','.allSels',function(){
	$(this).closest('.scrollCont1').find('input[type=checkbox]').attr('checked', this.checked);
});

$(document).on('click','.sels', function(){
	if($(this).closest('.scrollCont1').find('.sels').length == $(this).closest('.scrollCont1').find('.sels:checked').length) {
		$(this).closest('.scrollCont1').find('.allSels').attr("checked", "checked");
	} else {
		$(this).closest('.scrollCont1').find('.allSels').removeAttr("checked");
	}
});

/*$('.firstlev').on('click', function(e){
	e = e || event;
	e.cancelBubble = true;
	//e.stopImmediatePropagation();
	console.log(this);
	alert('hi')
	if($(this).is(':checked')){
		$(this).closest('.hd').next().find('input[type=checkbox]').attr('checked', true);
	}
	else{
		$(this).closest('.hd').next().find('input[type=checkbox]').attr('checked', false);
	}
});*/

$('#investmentId .planpanel .hd input[type=checkbox]').on('click',function(e){								 
	e.stopImmediatePropagation();			
	if($(this).attr('checked')){
		$(this).closest('.hd').next().find('input[type=checkbox]').attr("checked","checked");
	}else{
		$(this).closest('.hd').next().find('input[type=checkbox]').removeAttr("checked");		
	}
});

$('#byregion .planpanel .hd input[type=checkbox]').on('click',function(e){								 
	e.stopImmediatePropagation();			
	if($(this).attr('checked')){
		$(this).closest('.hd').next().find('input[type=checkbox]').attr("checked","checked");
	}else{
		$(this).closest('.hd').next().find('input[type=checkbox]').removeAttr("checked");		
	}		
});
$(document).on('click','.closeLink', function(e){
	e.preventDefault();
	$(this).closest('.alertInsti').removeClass('visible');
	$('.instPag').removeClass('closed');
});
	
$(document).on('click','#searchInstitute-btn1', function(e){
	$('.imageLoad').removeClass('closed');
	$('.alertInsti').removeClass('visible');
	setTimeout(function(){
		var len = $('#searchInstitute').val().length;
			leng = $('#searchInstitute').val(),
			instname = $('#InstName'),
			Iname = $('#searchInstitute').val();
			se = 0;
		if(len >= 3 && leng == "har"){
			$('.alertInsti').addClass('visible');
			$('.imageLoad').addClass('closed');
			$('#instiResults').removeClass('hidden');
			$(instname).removeClass('closed');
			pageSize = 10;
			showPage = function(page) {
			    $(".check").hide();
			    $(".check").each(function(n) {
			        if (n >= pageSize * (page - 1) && n < pageSize * page)
			            $(this).show();
			    });        
			}  
			showPage(1);
			$("#Instipagi a").click(function() {
				$('.imageLoad1').removeClass('closed');
				var relVal = $(this).attr('rel');
				setTimeout(function(){
					$("#Instipagi a").removeClass("current");
					$(this).addClass("current");
					if(relVal == '1'){
						$('.pageNum').html('1-10');
					}else{
						$('.pageNum').html('11-20');
					}
					$('.imageLoad1').addClass('closed');
				},2000);
				showPage(parseInt($(this).attr('rel'))) 
			});
			$(instname).find('input[type=checkbox]').attr('checked', false);
			$(instname).find('li').removeClass('matched').addClass('closed');
			//$(instname).find('li').addClass('closed');
			$(instname).find('li').each(function(){
				var ListName = $(this).text(),
					ser = ListName.toLowerCase().indexOf(Iname);
				if(ser >= 0){
				se++;
					$(this).addClass('matched');
					$('.matched').closest('li').removeClass('closed');
				}
				else{
					$(this).removeClass('matched');
				}
			});
		}else if(len >= 3){
			$('.imageLoad').addClass('closed');
			$('#instiResults').addClass('hidden');
			$(instname).removeClass('closed');
			$('#Instipagi').addClass('closed');
			$(instname).find('input[type=checkbox]').attr('checked', false);
			$(instname).find('li').removeClass('matched').addClass('closed');
			//$(instname).find('li').addClass('closed');
			$(instname).find('li').each(function(){
				var ListName = $(this).text();
					ser = ListName.toLowerCase().indexOf(Iname);
				if(ser >= 0){
				se++;
					$(this).addClass('matched');
					$('.matched').closest('li').removeClass('closed');
				}
				else{
					$(this).removeClass('matched');
				}
			});
		}else{
			e.preventDefault();
		} 
	}, 3000);
});

$(document).on('click','#applyInsti', function(e){
	var curr = $(this).closest('.content').prev().text();
	var rel = $(this).closest('.content').attr('rel');
	$('#InstName').find('input:checked').each(function(e){
		$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(this).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('.current').removeClass('closed');
	});
	$('#InstName').addClass('closed');
});
$(document).on('click','#cancelInsti', function(e){
	$('#InstName').find('input:checked').each(function(e){
		$(this).attr('checked', false);
	});
});

$(document).on('click','#applyLocation', function(e){
	//var curr = $(this).closest('.content').prev().text();
	var rel = $(this).closest('.content').attr('rel');
	if($('#state').is(':checked')== true){
		var curr = $('#state').val();
		if($('.states:checked').length > 0) {
			$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(".states").next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('.current').removeClass('closed');
		}else{
		$('.sels:checked').each(function(){
			$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(this).next().text()+'</span><span class="mls"></span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('.current').removeClass('closed');
		});
		}
	}else{
		$('#byregion').find('input:checked').each(function(e){
			var curr = $(this).closest('.content').prev().find('input[type=checkbox]').next().text();
			$('#currentSel').find('#rel'+rel).append('<li><span class="clsicn">'+curr+':<span class="mls"></span>'+$(this).next().text()+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
			$('.current').removeClass('closed');
		});
	}
});
$(document).on('click','#cancelLocation', function(e){
	if($('#state').is(':checked')== true){
		$('#bystate').find('input:checked').each(function(e){
			$(this).attr('checked', false);
		});
	}else{
		$('#byregion').find('input:checked').each(function(e){
			$(this).attr('checked', false);
		});
	}
});
});
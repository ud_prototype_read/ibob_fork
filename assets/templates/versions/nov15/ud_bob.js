// alert('hi---1');

//$(document).on('udPageReady', function(){
//	(console) && console.log("udPageReady",arguments);
//});
$(document).on('bodyLoaded', function(){
 
 		$("#reportgeneratebtn").attr("class","btn3");
		$("#reportgeneratebtn").attr("disabled",true);

         function removeli(id) {
					var ids="#rel"+id+" li"; 
					var liText = '', liList = $(ids), listForRemove = [];

					$(liList).each(function () {    
					var text = $(this).text();

					if (liText.indexOf('|'+ text + '|') == -1)
					liText += '|'+ text + '|';
					else
					listForRemove.push($(this));    
					});

					$(listForRemove).each(function () { $(this).remove(); });
         }

 
		 $('#generatereport input:checkbox').click(function(){
		 		reportgencheck();
		 });
 
		 function reportgencheck(){
				if ($("#generatereport input:checkbox:checked").length > 0){
				 		$("#reportgeneratebtn").attr("class","btn");
						$("#reportgeneratebtn").attr("disabled",false);
				}
				else{
				 		$("#reportgeneratebtn").attr("class","btn3");
						$("#reportgeneratebtn").attr("disabled",true);
				}		 	
		 }
	 
		var ie=$.browser.msie,
		ie6Flag = ($.browser.msie && $.browser.version.substr(0, 1) < 7) ? true : false,
		ie7Flag = ($.browser.msie && $.browser.version.substr(0, 1) < 8) ? true : false,
		ie8Flag = ($.browser.msie && $.browser.version.substr(0, 1) > 7) ? true : false,
		alertType = store.get('alerts'),
		chartType = store.get('charts');
 

		if(ie7Flag){   
				$("#current_selections").removeClass('shd2'); 
			  
				
		}else
		{  
				$("#current_selections").addClass('shd2'); 
			 
		}  

		if (document.all && !document.querySelector) {
				ie7Flag = true;
				$("#oppofirst").attr("class","three_sixth");
				$("#opposec").attr("class","");
				$("#oppothird").attr("class","");
				$("#riskfirst").attr("class","");
				$("#risklast").attr("class","");	
				//$("#interactionfirst").attr("class","two_third");
				//$("#interactionlast").attr("class","");
				//$("#interactionsec").attr("class","");					
		}


	    $(document).trigger('dc-after-load');

		var $links = $('#oppor_link1, #oppor_link2'),
			doc = $(document),
	        fc = $('.firstColumn'),
	        sc = $('.secondColumn'),
	        hf = $('.heightfix'),
	        mti = $('#myTabsId'),
	        mtd = $('#myTabsDiv'),
	        lb = $('#leftcol_bob'),
	        $wd = $('#wrapper'),
			$ssc = $('#showsidecolumn'),
			$tab = $('.myTab'),
			myCliHTML = '',
			$acs = $('#allClientStates'),
			$mcs = $('#myClientStates'),
			$mc = $('#myClients'),
			$mb	= $('.myBk'),
			$ap = $('#alertPopup'),
			$rdo1 = $('#radio1'),
			$rdo2 = $('#radio2'),
			$curSels = $('.curSels'),
			$current = $('.current'),
			$css = $('#curSelsSec'),
			$ics = $('#instCurSel'),
			$sdb = $('#setDfltBook'),
			$sl = $('.setLbl'),
			$mcl = $('#mngCustLst'),
			$mcl1 = $('#mngCustLst1'),
			$rb = $('#renameBook'),
			$cb = $('#copyBook'),
			$curS = $('#currentSel'),
			$as = $('#actionSelect'),
			$nonS = $('#noneSelection'),
			$selected = $('.selected'),
			$ifWin = $('#infodivWin'),
			$in = $('#InstName'),
			$infoH = $('.infoHover'),
			$wProb = $('#winProbability'),
			$range1 = $('#range1'),
			$range2 = $('#range2'),
			$editLink = $('.editLink');
			$tab.tabs();
		/*setting Layout starts here*/
	   
		/***************************Added for resizing *******************************/
		
		var wrap = $('#wrapper'),
			showcol = $('#showsidecolumn'),
			cont_scroll = $('.cont_scroll'),
			topH = $('#pagecontent').find('.pagetitle'),
			win = $(window);
		
		 
		$("#risk").on('click', function(e){
			$tab.tabs({ active: 3 })
		});

		$("#tranfers").on('click', function(e){
			$tab.tabs({ active: 6 })
		});	


		$("#distribution").on('click', function(e){
			$tab.tabs({ active: 7 });			 
		});

		$("#assetc").on('click', function(e){
			$tab.tabs({ active: 4 })
		});

		$("#interaction").on('click', function(e){
			$tab.tabs({ active: 1 })
		});
	 

 
		$("#ataglance").on('click', function(e){
			if($('#ataglance').attr('checked') == 'checked') {
				$('input[name="ataglance[]"]').each(function(){    
					this.checked = true;    
				});	
			}
			else{
				$('input[name="ataglance[]"]').each(function(){    
					this.checked = false;    
				});				
			}	
			reportgencheck();
		});
  
		$("#selectallTabs").on('click', function(e){ 		  
				if($('#selectallTabs').attr('checked') == 'checked') { 				
						$('#generatereport input[type="checkbox"]').each(function(){    
						    this.checked = true;    
						});
					 
				        $("#generatereport .panels .hd").each(function(){ 
							$(this).addClass('hdexpanded');
							$(this).find('a').addClass('expanded');
							$(this).next('div').addClass('bgexpanded');
							$(this).next('div').css('display','block'); 
						 
					    }); 
				}
				else{			
					$('#generatereport input[type="checkbox"]').each(function(){    
					    this.checked = false;    
					});
					 
				    $("#generatereport .panels .hd").each(function(){ 
						$(this).removeClass('hdexpanded');
						$(this).find('a').removeClass('expanded');
						$(this).find('a').addClass('collapsed');
						$(this).next('div').removeClass('bgexpanded');
						$(this).next('div').css('display','none'); 
				    });					 				
				}
				reportgencheck();		
		});
			 

		$("#interactionc").on('click', function(e){
			if($('#interactionc').attr('checked') == 'checked') {
				$('input[name="interaction[]"]').each(function(){    
				    this.checked = true;    
				});	
			}
			else{
				$('input[name="interaction[]"]').each(function(){    
				    this.checked = false;    
				});				
			}	
			reportgencheck();
		});


		$("#opportunity").on('click', function(e){
			if($('#opportunity').attr('checked') == 'checked') {
				$('input[name="opportunity[]"]').each(function(){    
				    this.checked = true;    
				});	
			}
			else{
				$('input[name="opportunity[]"]').each(function(){    
				    this.checked = false;    
				});				
			}	
			reportgencheck();
		});


		$("#riskrpt").on('click', function(e){
			if($('#riskrpt').attr('checked') == 'checked') {
				$('input[name="risk[]"]').each(function(){    
				    this.checked = true;    
				});	
			}
			else{
				$('input[name="risk[]"]').each(function(){    
				    this.checked = false;    
				});				
			}	
			reportgencheck();
		});


		$("#assets").on('click', function(e){
			if($('#assets').attr('checked') == 'checked') {
				$('input[name="assets[]"]').each(function(){    
				    this.checked = true;    
				});	
			}
			else{
				$('input[name="assets[]"]').each(function(){    
				    this.checked = false;    
				});				
			}
			reportgencheck();	
		});

		$("#contributions").on('click', function(e){
			if($('#contributions').attr('checked') == 'checked') {
				$('input[name="contributions[]"]').each(function(){    
				    this.checked = true;    
				});	
			}
			else{
				$('input[name="contributions[]"]').each(function(){    
				    this.checked = false;    
				});				
			}	
			reportgencheck();
		});
	

		$("#transfers").on('click', function(e){
			if($('#transfers').attr('checked') == 'checked') {
				$('input[name="transfers[]"]').each(function(){    
				    this.checked = true;    
				});	
			}
			else{
				$('input[name="transfers[]"]').each(function(){    
				    this.checked = false;    
				});				
			}
			reportgencheck();	
		});	

		$("#distributions").on('click', function(e){
			if($('#distributions').attr('checked') == 'checked') {
				$('input[name="distributions[]"]').each(function(){    
				    this.checked = true;    
				});	
			}
			else{
				$('input[name="distributions[]"]').each(function(){    
				    this.checked = false;    
				});				
			}	
			reportgencheck();
		});		

		$("#reportgeneratebtn").click(function(e){
			e.preventDefault();
			$('#generatereport').dialog('close');
		});

		setTimeout(function(){
			var secCW = wrap.width()-fc.outerWidth()- 62;
			if($(window).width()<1280){
				//sc.css('width','1000px').show();	
				
				cont_scroll.css('width','1200px').show();
			}else{
				//sc.css('width',secCW+'px').show();
				cont_scroll.css('width','100%').show();
			}
			//cont_scroll.css('width',secCW+'px').show();
			if($('#toolBarCheck').length > 0){
				var winH = win.height() - 122;
			}else if($('.pagetitle').find('header').css('display') == 'none'){
				var winH = win.height() -100;
			}else{
				var winH = win.height() -140;
			}
			//wrap.css('height', winH+'px');
			win.resize(function(){
				var secCW = wrap.width()-fc.outerWidth()- 60;
				if($(window).width()<1280){
					//sc.css('width','1000px').show();
					cont_scroll.css('width','1200px').show();	
				}else{
					//sc.css('width',secCW+'px').show();
					cont_scroll.css('width','100%').show();
				}
				//sc.css('width',secCW+'px').show();
				if($('#toolBarCheck').length > 0){
					var winH = win.height() - 122;
				}else if($('.pagetitle').find('header').css('display') == 'none'){
					var winH = win.height() -100;
				}else{
					var winH = win.height() -140;
				}
				//wrap.css('height',winH+'px');
			});
			
			fixedHeader();
		},20);
	 
		 fixedHeader();
	 	 $("#leftcol_bob .hd").mouseover(function() {
         $("div.firstColumn").getNiceScroll().resize();
         });
		  

		$(document).on('click','#hidesidecolumn, #showsidecolumn', function(e){
			e.preventDefault();
			if($(this).attr('id') == 'hidesidecolumn'){
				
			     fc.addClass('closed');
				 showcol.parent().removeClass('closed'); 
		         $('.secondColumn').css('margin-left','0px'); 
				 var secCW = wrap.width()-20;
				 //sc.css('width',secCW+'px');
				 
			}else{
				
				 fc.removeClass('closed');	

				 if(ie7Flag){
		         $('.secondColumn').css('margin-left','230px');
				 }
				 else
				 {
				 	$('.secondColumn').css('margin-left','220px');
				 }

				 showcol.parent().addClass('closed');
				 var secCW = wrap.width()-fc.outerWidth()-30;
				 //sc.css('width',secCW+'px');
			}
			fixedHeader();
		});
		 
		wrap.scroll(function(e){ 
			$('.infoHover, .actionsMenu').hide();
			fixedHeader();			
		});
		  

	  	 var variableHeight;
		 var WidDiv1Ht2; 
         variableHeight =  Math.abs(wrap.offset().top - $('.tableScrollXnot').offset().top-$('#myTabsDiv').find('#summary').offset().top-185);
			 
			  
			// console.log(wrap.offset().top+"=="+$('.tableScrollXnot').offset().top+"=="+$('#myTabsDiv').find('#summary').offset().top);
		   // ie7Flag ? (variableHeight) : (variableHeight = variableHeight-19); 
		 
		function fixedHeader(){  
			var tabId = $(".ui-tabs-active").attr('aria-controls'),
			myTabsIdHt = $('#myTabsId').find('ul').height(),
			WidDiv1Ht = $('#'+tabId).find('.panels').height(),
			summary = $('#myTabsDiv').find('#summary').height();
			 
			//console.log("tabId=="+tabId); 
			//var varhh=variableHeight-WidDiv1Ht-summary;  
			//console.log("myTabsIdHt="+myTabsIdHt+"WidDiv1Ht="+WidDiv1Ht+"summary="+summary+"variableHeight="+variableHeight+"varhh="+varhh);
			FinalHt = myTabsIdHt+WidDiv1Ht+summary+152;  
			if(ie7Flag){
				FinalHt = myTabsIdHt+WidDiv1Ht;
			}  
			var panelh=$('#'+tabId).find('.panels').height()-65;

			//console.log("panelh="+panelh);

			var wrapscroll;
			var addV;
			wrapscroll=wrap.scrollTop(); 
			//console.log("wrapscroll"+wrapscroll); 
			if(panelh==0){ 
				wrapscroll=wrap.scrollTop();
			 	wrapscroll=wrapscroll+310;
				FinalHt = variableHeight-panelh;
				addV=0;
				if(tabId=="Risk" || tabId=="Assets" || tabId=="Contributions" || tabId=="Transfers" || tabId=="Distributions")
				{
					  addV=10; 
				} 
				//console.log("wrapscrollTop"+wrapscroll+">"+variableHeight+"==="+FinalHt);
			}
			else
			{ 
				  wrapscroll=wrap.scrollTop(); 
			      addV=35; 
				  
				  if(tabId=="Risk" || tabId=="Assets" || tabId=="Contributions" || tabId=="Transfers" || tabId=="Distributions")
				  {
					  addV=48; 
				  }
			} 
			if(wrapscroll > variableHeight){ 
			 
			    if(ie || ie7Flag){   
				   $(document).find('.fixed_headers').css('position','absolute'); 
				   $(document).find('.fixed_headers').css({'top':(wrapscroll-FinalHt+addV)+'px'}); 
			}else
			{  
				  $(document).find('.fixed_headers').css('position','absolute'); 
				  $(document).find('.fixed_headers').css({'top':(wrapscroll-FinalHt+addV)+'px'});
			}  	
			}else{
				  $(document).find('.fixed_headers').css({'top':'0px'}); 
				  $(document).find('.fixed_headers').removeAttr('style');
				//$("#tbl_ibob thead").removeClass('fixed_headers');
			}			
		} 	
		 
		/***************************End of Added for resizing *******************************/
	 
	/* IE 6 & 7 popup open/close starts here */
    if ($('html').hasClass('ie7') == true || $('html').hasClass('ie6') == true) {
        $(document).on('click', 'a[href*="#"]', function(e) {
            var iePopups = $(this).attr('href');
            iePopups = '#' + iePopups.substring(iePopups.lastIndexOf('#') + 1);
            $(iePopups).dialog('open');
        });
    }
    /* IE 6 & 7 popup open/close ends here */
		
	/*Code for Selected Book starts here*/
	var $sel = $('.selBook');
	$(document).on('change', '#bookSelectList', function(e){
		if($(this).val() == 'ccb_1'){
			$sel.html('None');
		}else{
			$sel.html($(this).val());
		}		
	});
	/*Code for Selected Book ends here*/
	/*Code for My Clients and All Clients starts here*/
	$(document).on('click', '#radio1, #radio2', function(e) {
		if($(this).attr('id') == 'radio1'){
			$acs.addClass('closed');
			$mcs.removeClass('closed');
			$mc.append(myCliHTML);
			$mb.removeClass('closed');
		}else{
			$acs.removeClass('closed');
			$mcs.addClass('closed');
			myCliHTML = $mc.html();
			$mc.find('option').remove();
			$mb.addClass('closed');
		}
		if (($current.hasClass('closed') == false) || ($css.hasClass('closed') == false)) {
            $ap.dialog('open');
        }
    });
	/*Code for My Clients and All Clients ends here*/
	/*Code for Radio button Custome Book starts here*/
	$(document).on('click', '#cbradio1, #cbradio2, #cbradio3', function(e) {
        if ($(this).attr('id') == 'cbradio1') {
            $sdb.attr('disabled', true)
            $sl.addClass('dim');
        } else {
            $sdb.removeAttr('disabled');
            $sl.removeClass('dim');
        }
    });
	/*Code for Radio button Custome Book ends here*/
	/*Code for Radio button alert starts here*/
    $(document).on('click', '#cancelViewChange', function(e) {
        if ($rdo1.is(':checked') == true) {
            $rdo2.attr('checked', true);
        }
        if ($rdo2.is(':checked') == true) {
            $rdo1.attr('checked', true);
        }
    });	
    $(document).on('click', '#viewChange', function() {
		clearFunc();
        if ($rdo1.is(':checked') == true) {
            $rdo1.attr('checked', true);
        }
        if ($rdo2.is(':checked') == true) {
            $rdo2.attr('checked', true);           
        }
    });
	/*Code for Radio button alert ends here*/
    /*Code for Clear all alert ends here*/
	$(document).on('click', '#clearAllRef', function() {
			clearFunc();
    });
	/*Code for Clear all alert ends here*/
	function clearFunc(){
		$curS.find('ul').html('');
		$curSels.removeClass('closed');
        $nonS.removeClass('closed');
        $css.addClass('closed');
        $current.addClass('closed');
        $ics.html('None');
		//lb.find('input[type=checkbox]').attr('checked', false);
		//$('input:checkbox').removeAttr('checked');

		clearpopupChkbox()
	}
	 
	/*Code for Take action starts here*/
    $(document).on('change', '#actionSelect', function() {
        var copyBk = $mcl.find('.shdhlt').text(),
            renBk = $mcl.find('.shdhlt').text();
        if ($(this).find(":selected").text() == "Rename" && $('#radio1').is(':checked') == true) {
            $rb.removeClass('closed');
            $rb.val(renBk);
        } else if ($(this).find(":selected").text() == "Rename" && $('#radio2').is(':checked') == true) {
            $rb.removeClass('closed');
        } else {
            $rb.addClass('closed');
        }
        if ($(this).find(":selected").text() == "Make Copy" && $('#radio1').is(':checked') == true) {
            $cb.removeClass('closed');
            $cb.val('Copy of' + ' ' + copyBk);
        } else if ($(this).find(":selected").text() == "Make Copy" && $('#radio2').is(':checked') == true) {
            $cb.removeClass('closed');
        } else {
            $cb.addClass('closed');
        }
    });
	/*Code for Take action ends here*/
	/*Code for Create book ends here*/
    $(document).on('click', '#createBook', function() {
		$('#createBookPopup').dialog('open');
		var $csp = $('#curSelPopup');
        $csp.html('');
        var creatbook = $curS.html();
        $csp.append(creatbook);
    });
		/*Code for Create book ends here*/
    $(document).on('click', '#generatereportbtn', function() {
 
    	if ($('#rel4 li').length == 0) {
 
    		$("#contDaterange").attr('disabled', true);
    		$("#transferDaterange").attr('disabled', true);
    		$("#distrDaterange").attr('disabled', true);
			$("#transferDaterange2").attr('disabled', true);

    	}else
    	{ 
    		$("#contDaterange").prop( "checked", false );
    		$("#transferDaterange").prop( "checked", false );
    		$("#distrDaterange").prop( "checked", false );
            $("#transferDaterange2").prop( "checked", false );

    		$("#contDaterange").attr('disabled', false);
    		$("#transferDaterange").attr('disabled', false);
    		$("#distrDaterange").attr('disabled', false);
            $("#transferDaterange2").attr('disabled', false);
  
    	} 
		$('#generatereport').dialog('open'); 
    });
	 
	/*Code for Create book ends here*/
	/* Delete functionality for Current selection starts here*/
    $(document).on('click', '.icdeleteIcon', function() {
		var $li = $(this).closest('li'),
			$liC = $li.attr('class');
			$('li[data-class='+$liC+']').find('input').attr('checked', false);
		if($curS.find('li').length <= 2){
			$curS.closest('.scrollContainer').addClass('closed');
			$nonS.removeClass('closed');
            
            clearpopupChkbox();


		}else{
			$curS.closest('.scrollContainer').removeClass('closed');
			$nonS.addClass('closed');
		}
        $li.remove();
    });


	/* Delete functionality for Current selection ends here*/
	/*Code for Book selection on popup ends here*/
	$(document).on('click', '#mngCustLst .flevel li', function(e) {
        $(this).addClass('shdhlt').siblings().removeClass('shdhlt').closest('.flevel').siblings().find('li').removeClass('shdhlt');
        $as.removeAttr('disabled');
        if ($(this).hasClass('gblbk') == true) {
            $as.find('option:eq(2)').attr('disabled', true);
        } else {
            $as.find('option:eq(2)').removeAttr('disabled');
        }
        if ($(this).hasClass('gbldra') == true) {
            $as.find('option:eq(3)').removeAttr('disabled');
        } else {
            $as.find('option:eq(3)').attr('disabled', true);
        }
    });
	/*Code for Book selection on popup ends here*/
	/*Code for panel accordion starts here
    $('.hd').find('h3').on('click', function(e) {
        e.preventDefault();
		$infoH.addClass('hidden').removeClass('visible');
        if ($(this).closest('div').hasClass('hdexpanded') == true) {
            $(this).find('a').addClass('collapsed').removeClass('expanded');
            $(this).closest('div').next().addClass('expanded bgexpanded');
        } else {
            $(this).closest('div').next().addClass('expanded bgexpanded').siblings('.content').removeClass('expanded bgexpanded').hide();
            $(this).closest('div').next().siblings('.hd').removeClass('hdexpanded').find('a').addClass('collapsed').removeClass('expanded');
        }
    });
	Code for panel accordion ends here*/
	/*Code for secondlevel starts here*/
    $('#leftcol_bob .secondlevel').on('click', function(e) {
        if ($(this).attr('checked')) {
            $(this).closest('ul').parent().addClass('activeli');
            $(this).closest('content').parent().addClass('activeli');
        } else {
            $(this).closest('ul').parent().removeClass('activeli');
        }
    });
	/*Code for secondlevel ends here*/
	/*Select all functionality for My Team starts here*/
    $(document).on('click', '#selectAll', function() {
		var selAll = $('#selectAllOpen');
        if($(this).is(':checked')){
            selAll.find('input[type=checkbox]').attr('checked', true);
            selAll.find('input[type=checkbox]').trigger('click');
            selAll.find('.content1').addClass('closed');
            selAll.find('.clearspan').addClass('closed');
            selAll.find('.editLink').removeClass('closed');
            selAll.find('.firstlevel').attr('disabled', true);
			$curS.closest('.scrollContainer').removeClass('closed');
        }else {
            selAll.find('input[type=checkbox]').attr('checked', false);
			var rel = $(this).closest('.content').attr('rel');
			$curS.find('#rel'+rel).html('');
			if($curS.find('li').length < 2){
				$curS.closest('.scrollContainer').addClass('closed');
				setTimeout(function(){
					$nonS.removeClass('closed');
				},500);
			}
        }
    });
	/*Select all functionality for My Team starts here*/
	/*First Level Functionality starts here*/
    $('#leftcol_bob a.editLink').on('click', function(e) {
		
	  
        $(this).closest('li').find('.secondleveldiv').addClass('closed');
        $(this).closest('li').find('.secondleveldiv').removeClass('closed');

        $(this).closest('li').siblings().find('.content1').addClass('closed');

        //$(this).closest('li').siblings().addClass('shd3').find('.content1').addClass('closed');

        $(this).closest('.content').find('li.shd3').each(function(index) {
            $(this).removeClass('shd3');
            $(this).find('.clearspan').addClass('closed');
             $(this).find('.editLink').removeClass('closed');
            $(this).find('.secondleveldiv li').each(function(index) {
                if ($(this).hasClass('activeli') && $(this).parent().hasClass('nlist')) {
                    $(this).removeClass('closed');
                } else {
                    if ($(this).parent().hasClass('nlist')) {
                        $(this).addClass('closed');
                    }
                }
            });
        });


        $(this).closest('li').find('.clearspan').removeClass('closed');
         //$(this).closest('li').addClass('shd3');
        $(this).closest('li').find('.secondleveldiv').removeClass('closed');
        $(this).closest('li').find('.secondleveldiv').find('.secondlevel').removeClass('hidden');
        //$(this).addClass('closed');
        $(this).closest('li').find('.secondleveldiv li').each(function(index) {
            $(this).parent().parent().removeClass('closed');
            if ($(this).parent().hasClass('nlist')) {
                $(this).find('input[type=checkbox]').removeClass('closed');
                $(this).removeClass('closed');
            }
        });
        $(this).closest('li').find('.firstleveldiv').find('input[type=checkbox]').prop('disabled', false);
        $(this).closest('li').find('.secondleveldiv').find('.teamScroll').removeClass('closed');
		
			$("#assetCont li:last-child").removeClass('shd3');
			
			$("#oppor_link3").removeClass('closed');	

			$('#regionStateId_link1 .editLink').removeClass('closed');


    });
	/*First Level Functionality ends here*/
	/*First Level close Functionality starts here*/
    $('#leftcol_bob .icclearLink').on('click', function(e) {
        $(this).parent().parent().removeClass('shd3');
        $(this).parent().parent().find('.editLink').removeClass('closed');
        $(this).parent().addClass('closed');

        $(this).parent().parent().find('.secondleveldiv').addClass('closed');
        var ct = 0;
        $(this).parent().parent().find('.secondleveldiv li').each(function() {
            var chkbox = $(this).find('input[type=checkbox]').is(':checked'),
                radBtn = $(this).find('input[type=radio]').is(':checked');
            if (chkbox == true || radBtn == true) {
                ct++;
            }
        });
        $(this).closest('li').find('.firstleveldiv').find('input[type=checkbox]').prop('disabled', true);
        $(this).closest('li').find('.secondleveldiv').find('.teamScroll').addClass('closed');
    });
	/*First Level close Functionality ends here*/
	/*Parent Checkbox Selects All starts here*/
    $(document).on('change', '.firstlevel', function(e) {
        if ($(this).is(':checked')) {
			$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').trigger('click');
			setTimeout(function(){ 
				$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').attr('checked', true);
			}, 500);
            $('.secondlevel').closest('ul').parent().addClass('activeli');
        } else {
            $(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').prop('checked', false);
            $('.secondlevel').closest('ul').parent().removeClass('activeli');
        }
        $(window).resize();
    });
	/*Parent Checkbox Selects All ends here*/
	/*secondleveldiv checkbox functionality starts here*/
    var ct = 0;
    $('.secondleveldiv').find('input[type=checkbox]').on('click', function() {
        if ($(this).is(":checked") == false) {
            $(this).closest('.secondleveldiv').parent('li').find('.firstlevel').prop('indeterminate', true);
        }
    });
	/*secondleveldiv checkbox functionality starts here*/
	/*Interaction last visit starts here*/
	/*$('.rad').on('click', function(e) {
        $('.sel').remove();
		if ($(this).is(":checked") == true) {
			$(this).closest('ul').parent().addClass('activeli').siblings().addClass('activeli');
			var radio = $(this).next().text(),
				curr = $(this).closest('.content').prev().text();
				$curS.append('<li class="sel"><span class="clsicn">' + curr + ':<span class="mls"></span>' + radio + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
				$nonS.addClass('closed');
				$current.removeClass('closed');
		}else{
			$(this).closest('ul').parent().addClass('activeli').siblings().removeClass('activeli');
			$current.addClass('closed');
			$nonS.removeClass('closed');
		}        
            
    });*/

	/*Interaction last visit ends here*/
    $("#timeframe-slider").slider({
        orientation: "horizontal",
        range: true,
        value: 100,
        min: 0,
        max: 100,
        step: 1,
        values: [10, 30],
        slide: function(event, ui) {
            $("#range1").val(ui.values[0]);
            $("#range2").val(ui.values[1]);
        }
    });

 
	$('#btncancel4,#btnapply4').on("click", function(e){
		//hideInfoHover(this,'#ixiassets'); 			
		if($(this).attr('id')=="btnapply4"){
			$('#oppor_link3').closest('li').find('.nlist .np').removeClass('closed');
			var lbldata="$"+ $('#range9').val()+" to "+"$"+ $('#range10').val();
			
	 
			$('#oppor_link3_lbl').html(lbldata);				
			if(!$('#nonqual').attr('checked')){				
				updatefiltercount($('#nonqual'));
				$('#nonqual').attr('checked','checked');
			}	
		}
	});
		
		
    $(document).on('keyup', '#range1, #range2', function() {
        var $ran1 = $(this).val(),
            num = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/,
            slider = $(this).closest('.infoHover').find('.slider');
        if (!num.test($ran1) || $ran1 > 100) {
            $(this).addClass('descl');
        } else {
            $(this).removeClass('descl');
            if ($(this).attr('id') == 'range1') {
                slider.find('a').eq(0).css({'left': $ran1 + '%'});
            } else {
                slider.find('a').eq(1).css({'left': $ran1 + '%'});
            }
        }
        var r1 = $range1.val(),
            r2 = $range2.val(),
			rng = r1 - r2;
        if (r1 > r2) {
            $(this).closest('.infoHover').find('.ui-slider-range').css({'left': r2 + '%','width': rng + '%'});
        } else {
            $(this).closest('.infoHover').find('.ui-slider-range').css({'left': r1 + '%','width': rng + '%'});
        }
    });
	
	$('#applySlider').on("click", function(e) {
		$('.wProb').remove();
        $('#range1Val').html($range1.val());
        $('#range2Val').html($range2.val());
        $wProb.attr('checked', true);
        $ifWin.addClass('hidden').removeClass('visible');
        var title = $('.infoPopup').closest('li').text();
        $curS.append('<li class="wProb"><span>' + title + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span>');
        $current.removeClass('closed');
        //$wProb.prop('disabled', true);
        $wProb.closest('li').removeClass('shd3');
    });

    $('#leftcol_bob .firstleveldiv').on('click', function(e) {
        if ($(this).attr('id') == 'winProbability') {
            e.preventDefault();
        } else {
            $(this).closest('li').find('a.editLink').trigger('click');
        }
    });

    mtd.on('scroll', function() {
        $infoH.addClass('hidden').removeClass('visible');
    });

    $('.selview').on('click', function() {
        $('.tbldata').addClass('closed');
        $('li ul, li div.scrollContainer').addClass('closed');
        $('div.first h3 a').html('<span class="icon"></span>' + $(this).parent('li').find('span.txtlc').text() + ' Summary');
        if ($(this).attr('id') == 'Bob' || $(this).attr('id') == 'Market' || $(this).attr('id') == 'allClients') {
            $('#ibob').removeClass('closed');
        } else if ($(this).attr('id') == 'Region') {
            $('#regions').removeClass('closed');
        } else {
            $('#stat').removeClass('closed');
        }
        var subID = 'sub' + $(this).attr('id');
        if ($('#' + subID).length) {
            $('#' + subID + ',#' + subID + ' ul').removeClass('closed');
        }
    });

    $('.narview').on('click', function() {
        $('.subnarrow').addClass('closed');
        var subnarview = 'sub' + $(this).attr('id');
        $('.' + subnarview).removeClass('closed');
    });
 
	 
	 function hideInfoHover(objl, objdiv) {
	 $(objdiv).fadeOut('fast');
     } 

	/*Clear checkbox*/

     function clearpopupChkbox() {
	 var oppupIdchk=["institutionId","myTeamidOne","myTeamidtwo","marketId","segmentId","riskStatusId","topicId","subtopicId","channelId","lastVisitedId","statusId","transfersId","distributionId","assetClassId","investmentId","remittanceDate","opportunitytypeId","servicemodelId","salesphaseId","infodivWin","productandservicesId","oppstatusId"]
     
     for(var i=0;i<=oppupIdchk.length;i++){


     	$("#"+oppupIdchk[i]+' input[type=checkbox]').attr('checked',false)
     }

     }


	/* Current Selection None functionality ends here*/
	/* Callout functionality starts here*/
	
 	 function showInfoHover(objl, objdiv, pos) { 
    var oppupId=["institutionId","myTeamidOne","myTeamidtwo","marketId","segmentId","riskStatusId","topicId","subtopicId","channelId","lastVisitedId","statusId","transfersId","distributionId","assetClassId","investmentId","remittanceDate","opportunitytypeId","servicemodelId","salesphaseId","infodivWin","productandservicesId","oppstatusId"]
	 
    var newValue = objdiv.replace('#', '');  
  	$('.infoHover').hide();   
	var thisPos = $(objl).closest('li').offset();
	var leftPos = thisPos.left + $(objl).closest('li').width() + 25;  // px for padding, border
    var infoHoverHeight = $(objdiv).height();
    var topPos = thisPos.top-($(objdiv).height()/2); // px for dropdown icon width
	var wH = $(window).height(); 
	var dynPos = $(objl).offset().top+($(objdiv).height()/2);  
	var pntrOT=$(objdiv).height()/2;  
	var gtW = (dynPos-wH); 
	if(dynPos > wH){ 
		var pntrOT = (infoHoverHeight/2)+gtW;
		$(objdiv).css('top', (topPos-gtW) + 'px').css('left', (leftPos) + 'px').fadeIn('fast').addClass('visible').fadeIn('fast');
		$(objdiv).find('.pointer').css('top',(pntrOT+10)+'px');

	}else{ 
		$(objdiv).css('top', (topPos) + 'px').css('left', (leftPos) + 'px').fadeIn('fast').addClass('visible').removeClass('hidden').fadeIn('fast');
		$(objdiv).find('.pointer').css('top',(pntrOT+15)+'px'); 
	} 
	  
		 for(var i=0; i<=oppupId.length; i++){ 
         	if(oppupId[i]==newValue){   
         	}
         	else{ 
                 $("#"+oppupId[i]).addClass('closed').removeClass('visible'); 
         	} 
         }   
    }
 
	$(document).on('click', '#winCancel, #assetCancel', function(e) {
       $infoH.addClass('hidden').removeClass('visible');
    });
   										 
	$("#winProb").on('click',function(e) {											 
        e.preventDefault(); 
        $(this).closest('li').addClass('selected').siblings().removeClass('selected');
        $editLink.removeClass('closed');

        $("#infodivWin").addClass('visible').removeClass('hidden');
		showInfoHover(this, '#infodivWin');
    });
	



	$('#phOnList1 .hd input[type=checkbox]').on('click',function(e){									 
		 e.stopImmediatePropagation();
		 var maincheck=$(this);
		$(this).closest('.hd').next().find('input').each(function(index){
			if(maincheck.attr('checked')=='checked')	 {											  
				$(this).attr('checked',true);
			}
			else {
				$(this).removeAttr('checked');
			}				
		});
	});

	 	
 
	$("#institutionId_link1").on('click',function(e) {
	        if ($(this).hasClass('disabled') == true) {
	            e.preventDefault();
	            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

	        } else {

                e.stopPropagation();
		        e.preventDefault(); 
		        $("#institutionId").addClass('visible').removeClass('hidden');
	            showInfoHover(this, '#institutionId');
	            $editLink.removeClass('closed');
	            $('#institutionId').closest('li').removeClass('shd3');
	            return false;
	        } 
	});



		//$("body").on("click", "#institutionId_link1", function(e){															
        //e.stopPropagation();
		//e.preventDefault();		
        //showInfoHover(this,'#institutionId');
	//});	




 
 
	$("#myTeamone").on('click',function(e) {
	    if ($(this).hasClass('disabled') == true) {
	        e.preventDefault();
	        $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

	    } else {
	    	$("#myTeamidOne").addClass('visible').removeClass('hidden');
	        showInfoHover(this, '#myTeamidOne');
	        $editLink.removeClass('closed');
	        $('#myTeamidOne').closest('li').removeClass('shd3');
	        return false;
	    }  
	});
 


	$("#myTeamtwo").on('click',function(e) {
        if ($(this).hasClass('disabled') == true) {
            e.preventDefault();
            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

        } else {
        	$("#myTeamidtwo").addClass('visible').removeClass('hidden');
            showInfoHover(this, '#myTeamidtwo');
            $editLink.removeClass('closed');
            $('#myTeamidtwo').closest('li').removeClass('shd3');
            return false;
        }  
	});
  

	$("#regionStateId_link1").on('click',function(e) {
	        if ($(this).hasClass('disabled') == true) {
	            e.preventDefault();
	            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

	        } else {
	        	$("#regionStateId").addClass('visible').removeClass('hidden');
	            showInfoHover(this, '#regionStateId');
	            $editLink.removeClass('closed');
	            $('#regionStateId').closest('li').removeClass('shd3');
	            return false;
	        }   
	});
	 
	$("#marketId_link1").on('click',function(e) {
	        if ($(this).hasClass('disabled') == true) {
	            e.preventDefault();
	            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

	        } else {
	        	$("#marketId").addClass('visible').removeClass('hidden');
	            showInfoHover(this, '#marketId');
	            $editLink.removeClass('closed');
	            $('#marketId').closest('li').removeClass('shd3');
	            return false;
	        } 


	});
 
	$("#segmentId_link1").on('click',function(e) {
        if ($(this).hasClass('disabled') == true) {
            e.preventDefault();
            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

        } else {
        	$("#segmentId").addClass('visible').removeClass('hidden');
            showInfoHover(this, '#segmentId');
            $editLink.removeClass('closed');
            $('#segmentId').closest('li').removeClass('shd3');
            return false;
        }   
	});
	 
	$("#riskStatusId_link1").on('click',function(e) {
	        if ($(this).hasClass('disabled') == true) { 
	            e.preventDefault();
	            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
	        } else {
	        	$("#riskStatusId").addClass('visible').removeClass('hidden');
	            showInfoHover(this, '#riskStatusId');
	            $editLink.removeClass('closed');
	            $('#riskStatusId').closest('li').removeClass('shd3');
	            return false;
	        }   
	});
 
	$("#topicId_link1").on('click',function(e) {
	        if ($(this).hasClass('disabled') == true) { 
	            e.preventDefault();
	            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
	        } else {
	        	$("#topicId").addClass('visible').removeClass('hidden');
	            showInfoHover(this, '#topicId');
	            $editLink.removeClass('closed');
	            $('#topicId').closest('li').removeClass('shd3');
	            return false;
	        }   
	});
	 
	$("#subtopicId_link1").on('click',function(e) {
	        if ($(this).hasClass('disabled') == true) { 
	            e.preventDefault();
	            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
	        } else {
	        	$("#subtopicId").addClass('visible').removeClass('hidden');
	            showInfoHover(this, '#subtopicId');
	            $editLink.removeClass('closed');
	            $('#subtopicId').closest('li').removeClass('shd3');
	            return false;
	        }     
	});
 
	$("#channelId_link1").on('click',function(e) {
			if ($(this).hasClass('disabled') == true) { 
				e.preventDefault();
				$(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
			} else {
				$("#channelId").addClass('visible').removeClass('hidden');
				showInfoHover(this, '#channelId');
				$editLink.removeClass('closed');
				$('#channelId').closest('li').removeClass('shd3');
				return false;
			}    
	});
 
	$("#lastVisitedId_link1").on('click',function(e) {
		    if ($(this).hasClass('disabled') == true) { 
		        e.preventDefault();
		        $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
		    } else {
		    	$("#lastVisitedId").addClass('visible').removeClass('hidden');
		        showInfoHover(this, '#lastVisitedId');
		        $editLink.removeClass('closed');
		        $('#lastVisitedId').closest('li').removeClass('shd3');
		        return false;
		    }     
	});
 
	$("#statusId_link1").on('click',function(e) {
		    if ($(this).hasClass('disabled') == true) { 
		        e.preventDefault();
		        $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
		    } else {
		    	$("#statusId").addClass('visible').removeClass('hidden');
		        showInfoHover(this, '#statusId');
		        $editLink.removeClass('closed');
		        $('#statusId').closest('li').removeClass('shd3');
		        return false;
		    }   
	});
	
	
	
        $("#opportunitytypeId_link1").on('click',function(e) {
			if ($(this).hasClass('disabled') == true) { 
				e.preventDefault();
				$(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
			} else {
				$("#opportunitytypeId").addClass('visible').removeClass('hidden');
				showInfoHover(this, '#opportunitytypeId');
				$editLink.removeClass('closed');
				$('#opportunitytypeId').closest('li').removeClass('shd3');
				return false;
			}    
	});
		
		  $("#servicemodel_link").on('click',function(e) {
			if ($(this).hasClass('disabled') == true) { 
				e.preventDefault();
				$(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
			} else {
				$("#servicemodelId").addClass('visible').removeClass('hidden');
				showInfoHover(this, '#servicemodelId');
				$editLink.removeClass('closed');
				$('#servicemodelId').closest('li').removeClass('shd3');
				return false;
			}    
	});
	
	
		  $("#salesphase_link").on('click',function(e) {
			if ($(this).hasClass('disabled') == true) { 
				e.preventDefault();
				$(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
			} else {
				$("#salesphaseId").addClass('visible').removeClass('hidden');
				showInfoHover(this, '#salesphaseId');
				$editLink.removeClass('closed');
				$('#salesphaseId').closest('li').removeClass('shd3');
				return false;
			}    
	});
	
	 
		 $("#productandservices_link").on('click',function(e) {
			if ($(this).hasClass('disabled') == true) { 
				e.preventDefault();
				$(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
			} else {
				$("#productandservicesId").addClass('visible').removeClass('hidden');
				showInfoHover(this, '#productandservicesId');
				$editLink.removeClass('closed');
				$('#productandservicesId').closest('li').removeClass('shd3');
				return false;
			}    
	});
	 
	 
	 	$("#oppstatusId_link1").on('click',function(e) {
		    if ($(this).hasClass('disabled') == true) { 
		        e.preventDefault();
		        $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed'); 
		    } else {
		    	$("#oppstatusId").addClass('visible').removeClass('hidden');
		        showInfoHover(this, '#oppstatusId');
		        $editLink.removeClass('closed');
		        $('#oppstatusId').closest('li').removeClass('shd3');
		        return false;
		    }   
	});
		
		
	$("#transfers_link1").on('click',function(e) {
	        if ($(this).hasClass('disabled') == true) {
	            e.preventDefault();
	            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

	        } else {
	        	$("#transfersId").addClass('visible').removeClass('hidden');
	            showInfoHover(this, '#transfersId');
	            $editLink.removeClass('closed');
	            $('#transfersId').closest('li').removeClass('shd3');
	            return false;
	        }  
	});
 
 	$("#distribution_link1").on('click',function(e) {
        if ($(this).hasClass('disabled') == true) { 
            e.preventDefault();
            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

        } else {
        	$("#distributionId").addClass('visible').removeClass('hidden');
            showInfoHover(this, '#distributionId');
            $editLink.removeClass('closed');
            $('#distributionId').closest('li').removeClass('shd3');
            return false;
        } 
	});
	
 
 	$("#dateRange_link3").on('click',function(e) {
		var curDate = new Date(),
		curM = curDate.getMonth()+1,
		curY = curDate.getFullYear(),
		preY = curY - 1;
		$('#year').find('option:eq(0)').val(preY).text(preY);
		$('#year').find('option:eq(1)').val(curY).text(curY);
		$('#year1').find('option:eq(0)').val(preY).text(preY);
		$('#year1').find('option:eq(1)').val(curY).text(curY);
		$('#month1').val(curM);
		$('#month').val(curM);
		$('#month1').find('option').removeAttr('disabled');
		$('#month1').find('option').each(function(){
		if($(this).val() > curM){
		$(this).attr('disabled',true);
		}
		});
		$('#month').find('option').each(function(){
		if($(this).val() < curM){
		$(this).attr('disabled',true);
		}
		});
		$('#remittanceDate').addClass('visible').removeClass('hidden');

	    if ($(this).hasClass('disabled') == true) {

	        e.preventDefault();
	        $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');

	    } else {
	    	$("#remittanceDate").addClass('visible').removeClass('hidden');
	        showInfoHover(this, '#remittanceDate');
	        $editLink.removeClass('closed');
	        $('#remittanceDate').closest('li').removeClass('shd3');
	        return false;
	    }  
	});
	
 

    $('.btncancel').on('click', function(e){
		$('.infoPopup').closest('li').removeClass('shd3');
		$('.infoHover').addClass('closed').removeClass('visible');
		$('.infoPopup1').closest('li').removeClass('shd3');
		$('#assetClass1').prop('disabled', true);
		$('#date').prop('disabled', true);
	});


	$("#applyDate").on('click',function(e) {
		$('.Otd').remove();
		var month = $('#month').find('option:selected').text(),
		month_val = $('#month').val(),
		year = $('#year').find('option:selected').text(),
		year_val = $('#year').val(),
		month1 = $('#month1').find('option:selected').text(),
		month1_val = $('#month1').val(),
		year1 = $('#year1').find('option:selected').text(),
		year1_val = $('#year1').val(),

		//curr = $('#oppor_link3').closest('.selected').find('label').text(),

		//rel = $('#oppor_link3').closest('.content').attr('rel'),


		curr = $(this).attr('curr'),
		rel = $(this).attr('rel'),
		idBtn = $(this).attr('id'),
		popupId = $(this).attr('popupid'), 
		checkedId="#"+popupId+" .checked",
		checkboxId=$(this).attr('chkbtn'),
		lnk = $(this).next().text().replace(/[^A-Z0-9]+/ig, "_");

		$( "#"+checkboxId).prop( "checked", true );

		res_year = year1_val- year_val,
		res_month = month1_val- month_val;
		if(res_month>= 1 && res_year <= 1 ){
			res_year = res_year + 1;
		}else{}
		if( res_year < 2){
		if(month_val < month1_val && year_val <= year1_val){
		$('#currentSel').find('#rel'+rel).append('<li class="Otd" class="'+lnk+'"><span class="clsicn">'+curr+':<span class="mls"></span>'+month+' '+year+' to<br>'+month1+' '+year1+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('#dateRange').addClass('hidden');
		$('.current').removeClass('closed');
		$('.infoHover').addClass('closed').removeClass('visible');
		$('#date').prop('checked', true);
		//$('#date').prop('disabled', true);
		}else if(year_val < year1_val && month_val >= month1_val){
		$('#currentSel').find('#rel'+rel).append('<li class="Otd" class="'+lnk+'"><span class="clsicn">'+curr+':<span class="mls"></span>'+month+' '+year+' to<br>'+month1+' '+year1+'</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
		$('#dateRange').addClass('hidden');
		$('.current').removeClass('closed');
		$('.infoHover').addClass('closed').removeClass('visible');
		$('#date').prop('checked', true);
		//$('#date').prop('disabled', true);
		}else{
		$('#dateRange').removeClass('hidden');
		}
		}else{
			$('#dateRange').removeClass('hidden');
		}

		removeli(rel);
	});
	
	
 

	$("#oppor_link1").on('click',function(e) {
			if ($(this).hasClass('disabled') == true) {
				e.preventDefault();
				$(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');
			} else {
				$('#assetClassId').addClass('visible').removeClass('hidden');
				showInfoHover(this, '#assetClassId');
				$editLink.removeClass('closed');
				$('#oppor_link1').closest('li').removeClass('shd3');
				$('#investmentId').addClass('hidden');
				//$('#ixiassets').addClass('hidden');
				$('#transferVendor').addClass('hidden');
				return false;
			}
	});
 
	$("#oppor_link2").on('click',function(e) {
		if ($(this).hasClass('disabled') == true) {
		    e.preventDefault();
		    $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');
		} else {
		    $('#investmentId').addClass('visible').removeClass('hidden');
		    showInfoHover(this, '#investmentId');
		    $editLink.removeClass('closed');
		    $('#oppor_link2').closest('li').removeClass('shd3');
		    $('#assetClassId').addClass('hidden');
			//$('#ixiassets').addClass('hidden');
		    $('#transferVendor').addClass('hidden');
		    return false;
		}
	});
		
 

	$("#oppor_link3").on('click',function(e) {
			$("#assetCont li:last-child").removeClass('shd3');
		
	        if ($(this).hasClass('disabled') == true) {
	            e.preventDefault();
	            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');
	        } else {
		           // $('#ixiassets').addClass('visible').removeClass('hidden');
		           // showInfoHover(this, '#ixiassets');
		            $editLink.removeClass('closed');
		            $('#oppor_link3').closest('li').removeClass('shd3');
					
				    $("#nonqual").closest('li').removeClass('shd3');
					 
		            $('#investmentId').addClass('hidden');
					$('#assetClassId').addClass('hidden'); 
		            $('#transferVendor').addClass('hidden');
		            return false;
  			  } 
	});
	
  
 	$('#nondirect').on('click', function(e) { 
    if ($(this).is(":checked") == true) {  
	$("#segmentchkbox input[type=checkbox]").attr('checked',true);  
    }else{ 
    $("#segmentchkbox input[type=checkbox]").attr('checked',false);  
    } 
 	});



	$(document).on("click", "#assetCont li:last-child", function(e) { 
		$(this).removeClass('shd3'); 
	});
	  	    
	/* Callout functionality ends here*/
    $('.btncancel').on('click', function(e) {
        $('.infoPopup').closest('li').removeClass('shd3');
        $('.infoHover').addClass('closed').removeClass('visible');
        $('#assetClass1').prop('disabled', true);
        $('#date').prop('disabled', true);
    });

    var cntnl = 0;
 

    var ct = 0;
    $('.btnapply').on('click', function(e) {
        $selected.find('input[type=checkbox]').attr('checked', false);
        $selected.find('.append').html('');
        var current = ($selected.find('input[type=checkbox]').next().text());
        $(this).closest('.infoHover').find('input:checked').each(function(e, obj) {
            ct++;
            $ics.html(ct + ' Institutions meet current selections:');
            $selected.find('.append').append($(obj).next().text() + '<br />');
            $curS.append('<li><span>' + current + ':' + $(obj).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
        });
        $selected.find('.append').removeClass('closed');
        $('.infoHover').addClass('closed').removeClass('visible');
        $('.infoPopup').closest('li').removeClass('shd3');
        $selected.find('input[type=checkbox]').attr('checked', true);
        $current.removeClass('closed');
    });
    $(document).on('click', '.allSel', function() {
        $(this).closest('ul').find('.sel').attr('checked', this.checked);
    });
    $(document).on('click', '.sel', function() {
        if ($(this).closest('div').find('.sel').length == $(this).closest('div').find('.sel:checked').length) {
            $(this).closest('div').find('.allSel').attr("checked", "checked");
        } else {
            $(this).closest('div').find('.allSel').removeAttr("checked");
        }
    });
 
	$('.currentSelection').on("click", function(e) {
	 
	     var curr = $(this).attr('curr');
         var rel = $(this).attr('rel'); 
         var idBtn = $(this).attr('id');
         var popupId = $(this).attr('popupid'); 
         var checkedId="#"+popupId+" .checked";
         var checkboxId=$(this).attr('chkbtn');
		  
         $( "#"+checkboxId).prop( "checked", true );
 
            $(checkedId).each(function() {  
                 	if(this.checked) { 
                     var lnk = $(this).next().text().replace(/[^A-Z0-9]+/ig, "_");
            		 $curS.find('#rel' + rel).append('<li class="'+lnk+'"><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="mls"></span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                     $current.removeClass('closed');
                     removeli(rel);  
                     }else
                     { 
                     var lnk = $(this).next().text().replace(/[^A-Z0-9]+/ig, "_"); 
                     $('.'+lnk).remove(); 
                     removeli(rel); 
 
                     if($curS.find('li').length < 2){
			          $curS.closest('.scrollContainer').addClass('closed');
			          $nonS.removeClass('closed');
		             }

                     } 
 
            });  

              hideInfoHover(this,'#'+popupId);  

	});
	 

    $('#btnapply').on("click", function(e) {
        var curr = $('#investments').next().text();
        var rel = $('#oppor_link2').closest('.content').attr('rel');
        if ($('.fcompany:checked').length > 0) {
            $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(".fcompany").next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
            $current.removeClass('closed');
        } else if ($('.iType:checked').length > 0) {
            $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(".iType").next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
            $current.removeClass('closed');
        } else {
            $('.sels:checked').each(function() {
                $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                $current.removeClass('closed');
            });
        }
        $('#investments').prop('checked', true);
        $('#investments').prop('disabled', true);
        $('#investments').closest('li').removeClass('shd3');
    });

    $(document).on("change", '#year, #yearDis', function(e) {
        var curDate = new Date(),
            curM = curDate.getMonth() + 1,
            curY = curDate.getFullYear(),
            preY = curY - 1;
        if ($(this).attr('id') == 'year') {
            if ($(this).val() == preY) {
                $('#month').val(curM);
                $('#month').find('option').removeAttr('disabled');
                $('#month').find('option').each(function() {
                    if ($(this).val() < curM) {
                        $(this).attr('disabled', true);
                    }
                });
            } else {
                $('#month').val(curM);
                $('#month').find('option').removeAttr('disabled');
                $('#month').find('option').each(function() {
                    if ($(this).val() > curM) {
                        $(this).attr('disabled', true);
                    }
                });
            }
        } else {
            if ($(this).val() == preY) {
                $('#monthDis').val(curM);
                $('#monthDis').find('option').removeAttr('disabled');
                $('#monthDis').find('option').each(function() {
                    if ($(this).val() < curM) {
                        $(this).attr('disabled', true);
                    }
                });
            } else {
                $('#monthDis').val(curM);
                $('#monthDis').find('option').removeAttr('disabled');
                $('#monthDis').find('option').each(function() {
                    if ($(this).val() > curM) {
                        $(this).attr('disabled', true);
                    }
                });
            }
        }
    });

    $(document).on("change", '#year1, #year1Dis', function(e) {
        var curDate = new Date(),
            curM = curDate.getMonth() + 1,
            curY = curDate.getFullYear(),
            preY = curY - 1;
        if ($(this).attr('id') == 'year1') {
            if ($(this).val() == preY) {
                $('#month1').val(curM);
                $('#month1').find('option').removeAttr('disabled');
                $('#month1').find('option').each(function() {
                    if ($(this).val() < curM) {
                        $(this).attr('disabled', true);
                    }
                });
            } else {
                $('#month1').val(curM);
                $('#month1').find('option').removeAttr('disabled');
                $('#month1').find('option').each(function() {
                    if ($(this).val() > curM) {
                        $(this).attr('disabled', true);
                    }
                });
            }
        } else {
            if ($(this).val() == preY) {
                $('#month1Dis').val(curM);
                $('#month1Dis').find('option').removeAttr('disabled');
                $('#month1Dis').find('option').each(function() {
                    if ($(this).val() < curM) {
                        $(this).attr('disabled', true);
                    }
                });
            } else {
                $('#month1Dis').val(curM);
                $('#month1Dis').find('option').removeAttr('disabled');
                $('#month1Dis').find('option').each(function() {
                    if ($(this).val() > curM) {
                        $(this).attr('disabled', true);
                    }
                });
            }
        }
    });

    $(document).on('click', '#fundComp', function(e) {
        $('#FundCompany').removeClass('closed');
        $('#InvestType').addClass('closed');
    });

    $(document).on('click', '#investType', function(e) {
        $('#InvestType').removeClass('closed');
        $('#FundCompany').addClass('closed');
    });

    $('.thirdlev').on('change', function() {
        if ($(this).closest('ul').find('input[type=checkbox]').length == $(this).closest('ul').find('input:checked').length) {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', true)
        } else {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', false);
        }
    });

    $('.sublev').on('change', function() {
        if ($(this).closest('.content').find('input[type=checkbox]').length == $(this).closest('.content').find('input:checked').length) {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', true)
        } else {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', false);
        }
    });

    $('.sublev1').on('change', function() {
        if ($(this).closest('.content').find('input[type=checkbox]').length == $(this).closest('.content').find('input:checked').length) {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', true)
        } else {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', false);
        }
    });

    $(document).on('click', '.allSels', function() {
        $(this).closest('.scrollCont1').find('input[type=checkbox]').attr('checked', this.checked);
    });

    $(document).on('click', '.sels', function() {
        if ($(this).closest('.scrollCont1').find('.sels').length == $(this).closest('.scrollCont1').find('.sels:checked').length) {
            $(this).closest('.scrollCont1').find('.allSels').attr("checked", "checked");
        } else {
            $(this).closest('.scrollCont1').find('.allSels').removeAttr("checked");
        }
    });

    $('#investmentId .planpanel .hd input[type=checkbox]').on('click', function(e) {
        e.stopImmediatePropagation();
        if ($(this).attr('checked')) {
            $(this).closest('.hd').next().find('input[type=checkbox]').attr("checked", "checked");
        } else {
            $(this).closest('.hd').next().find('input[type=checkbox]').removeAttr("checked");
        }
    });

    $('#byregion .planpanel .hd input[type=checkbox]').on('click', function(e) {
        e.stopImmediatePropagation();
        if ($(this).attr('checked')) {
            $(this).closest('.hd').next().find('input[type=checkbox]').attr("checked", "checked");
        } else {
            $(this).closest('.hd').next().find('input[type=checkbox]').removeAttr("checked");
        }
    });

    $(document).on('click', '.closeLink', function(e) {
        e.preventDefault();
        $(this).closest('.alertInsti').removeClass('visible');
        $('.instPag').removeClass('closed');
    });

    $(document).on('click', '#searchInstitute-btn1', function(e) {
        $('.imageLoad').removeClass('closed');
        $('.alertInsti').removeClass('visible');
        setTimeout(function() {
            var len = $('#searchInstitute').val().length;
            leng = $('#searchInstitute').val(),
            instname = $('#InstName'),
            Iname = $('#searchInstitute').val();
            se = 0;
            if (len >= 3 && leng == "har") {
                $('.alertInsti').addClass('visible');
                $('.imageLoad').addClass('closed');
                $('#instiResults').removeClass('hidden');
                $(instname).removeClass('closed');
                pageSize = 10;
                showPage = function(page) {
 
                            $("#InstName .check").removeClass("visible");
                            $("#InstName  .check").addClass("closed");
                            $("#InstName  .check").css('display','none');


                    $("#InstName .check").each(function(n) {

                    	console.log(n +"="+ pageSize +"*"+ (page - 1) +"&&"+ n +"<"+ pageSize +"*"+ page)

                        if (n >= pageSize * (page - 1) && n < pageSize * page)
                        { 
                            $(this).removeClass("closed");
                            $(this).addClass("visible");
                            $(this).css('display','block');
                           }

                    });
                }


                showPage(1);
                $("#Instipagi a").click(function() {
                    $('.imageLoad1').removeClass('closed');
                    var relVal = $(this).attr('rel');
                    setTimeout(function() {
                        $("#Instipagi a").removeClass("current");
                        $(this).addClass("current");
                        if (relVal == '1') {
                            $('.pageNum').html('1-10');
                        } else {
                            $('.pageNum').html('11-20');
                        }
                        $('.imageLoad1').addClass('closed');
                    }, 1000);
                    showPage(parseInt($(this).attr('rel')))
                });
                $(instname).find('input[type=checkbox]').attr('checked', false);
                $(instname).find('li').removeClass('matched').addClass('closed');
                $(instname).find('li').each(function() {
                    var ListName = $(this).text(),
                        ser = ListName.toLowerCase().indexOf(Iname);
                    if (ser >= 0) {
                        se++;
                        $(this).addClass('matched');
                        $('.matched').closest('li').removeClass('closed');
                    } else {
                        $(this).removeClass('matched');
                    }
                });
            } else if (len >= 3) {
                $('.imageLoad').addClass('closed');
                $('#instiResults').addClass('hidden');
                $(instname).removeClass('closed');
                $('#Instipagi').addClass('closed');

                $(instname).find('input[type=checkbox]').attr('checked', false);

                $(instname).find('li').removeClass('matched').addClass('closed');
                $(instname).find('li').each(function() {
                    var ListName = $(this).text();
                    ser = ListName.toLowerCase().indexOf(Iname);
                    if (ser >= 0) {
                        se++;
                        $(this).addClass('matched');
                        $('.matched').closest('li').removeClass('closed');
                    } else {
                        $(this).removeClass('matched');
                    }
                });
            } else {
                e.preventDefault();
            }
        }, 3000);
    });

    //$(document).on('click', '#applyInsti', function(e) { 

      		 // $( "#assetClass1" ).prop( "checked", true );
      		  //$infoH.addClass('hidden').removeClass('visible'); 


   // }); 


    $(document).on('click', '#cancelInsti', function(e) { 
    	var popupidAttr=$(this).attr('popupid');
        $in.find('input:checked').each(function(e) {
            $(this).attr('checked', false);
        }); 
        var pId="#"+popupidAttr;
        hideInfoHover(this,pId); 
    });
 
    $('#leftcol_bob input[type=checkbox]').change(function() { 
					var popupId = $(this).attr('chpopupid'); 
					
					
					if ($(this).is(':checked')) {
					
					}else{
						
					$('#'+popupId+' input[type=checkbox]').attr('checked',false);	 
					
					}
                      
    });
	 
    $(document).on('click', '#applyLocation', function(e) {
         
         var curr = $(this).attr('curr');
         var rel = $(this).attr('rel'); 
         var idBtn = $(this).attr('id');
         var popupId = $(this).attr('popupid');  

         if ($('#state').is(':checked') == true) {
            var curr = $('#state').val();
            if ($('.states:checked').length > 0) {

                var lnk = $(this).next().text().replace(/[^A-Z0-9]+/ig, "_");  
                $curS.find('#rel' + rel).append('<li class="'+lnk+'"><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(".states").next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                $current.removeClass('closed');
                removeli(rel); 
            } else {
                $('.sels:checked').each(function() {
                	var lnk = $(this).next().text().replace(/[^A-Z0-9]+/ig, "_");
                    $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="mls"></span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                    $current.removeClass('closed');
                    removeli(rel); 
                });
            }
         } else {
             $('#byregion').find('input:checked').each(function(e) {
                //var curr = $(this).closest('.content').prev().find('input[type=checkbox]').next().text();
 
                $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                $current.removeClass('closed');
                removeli(rel); 
            });
         }

	        $('.infoHover').addClass('closed').removeClass('visible');   
	        $('.infoHover').css('display','none');  
	        $( "#regionState" ).prop( "checked", true ); 
           //$('#'+popupId+' input[type=checkbox]').attr('checked',false)   
    });
 
    $(document).on('click', '#cancelLocation', function(e) {
        if ($('#state').is(':checked') == true) {
            $('#bystate').find('input:checked').each(function(e) {
                $(this).attr('checked', false);
            });
        } else {
            $('#byregion').find('input:checked').each(function(e) {
                $(this).attr('checked', false);
            });
        }
    });
	
	$(document).on('click', '#manageBook', function(e) {
		e.preventDefault();
		$('#manageBookPopup').dialog('open');
		$('#alertModule').addClass('hidden').removeClass('visible');
		$('#actionSelect').val('sel').attr('disabled', true);
		$('#renameBook').addClass('closed');
		$('#mngCustLst').find('li').removeClass('shdhlt');
    });
	
	$tab.find('li').on('click', function(e) {
		if ($(this).index() == 6) {
		    $links.addClass('disabled').closest('li').find('label').addClass('dim');
        } else {
            $links.removeClass('disabled').closest('li').find('label').removeClass('dim');
        }
    });
	
	var userInfo = store.get('user');
	
	$(document).on('click', '#state', function() {
		$('#bystate').removeClass('closed');
		$('#byregion').addClass('closed');
		if (userInfo == "UWC") {
			$('#myClientStates').addClass('closed');
			$('#allClientStates').removeClass('closed');
		}else{
			$('#myClientStates').removeClass('closed');
			$('#allClientStates').addClass('closed');
		}
	});
	
	$(document).on('click', '#region', function() {
		$('#bystate').addClass('closed');
		$('#myClientStates').addClass('closed');
		$('#byregion').removeClass('closed');
		$('#allClientStates').addClass('closed');
	});
	
	$(document).on('click', '#delBook', function() {
		var delItem = $('#mngCustLst').find('.shdhlt').attr('rel');
		$('#mngCustLst').find('.shdhlt').remove();
		$('#bookSelectList').find('option[rel=' + delItem + ']').remove();
		$('#deleteBook').dialog('close');
		$('#manageBookPopup').dialog('close');
		$('.selBook').html('None');
	});
	
	$(document).on('click', '#applyMngCust', function(e) {
		var actSel = $('#actionSelect').find(":selected").text(),
			renBk = $('#renameBook').val();
		if(actSel == "Rename" && $('#radio1').is(':checked') == true){
			if(renBk != ''){
				var renItem = $('#mngCustLst').find('.shdhlt').attr('rel');
				$('#mngCustLst').find('.shdhlt').html('<span>' + renBk + '</span>');
				$('#myClients').find('option[rel=' + renItem + ']').text(renBk).attr('selected', true);
				$('.selBook').html(renBk);
				$('#manageBookPopup').dialog('close');
			}else{
				$('#alertModule').addClass('visible').removeClass('hidden');
			}
		}else if(actSel == "Rename" && $('#radio2').is(':checked') == true){
			if(renBk != ''){
				var renItem = $('#mngCustLst').find('.shdhlt').attr('rel');
				$('#mngCustLst').find('.shdhlt').html('<span>' + renBk + '</span>');
				$('#allClients').find('option[rel=' + renItem + ']').text(renBk).attr('selected', true);
				$('.selBook').html(renBk);
				$('#manageBookPopup').dialog('close');
			}else{
				$('#alertModule').addClass('visible').removeClass('hidden');
			}
		}else if(actSel == "Set As Default Book"){
			var mcl = $('#mngCustLst1').find('.shdhlt').text();
			if($('#radio1').is(':checked') == true){
				$('#myClients').append('<option selected="selected">' + mcl + '</option>');
			}else{
				$('#allClients').append('<option selected="selected">' + mcl + '</option>');
			}
			$('#manageBookPopup').dialog('close');
		}else if(actSel == "Delete"){
			$('#bnam').html('');
			var delItem = $('#mngCustLst').find('.shdhlt').html();
			$('#bnam').append(' ' + delItem);
			$('#bnam').removeClass('closed');
			$('#deleteBook').dialog('open');
			$('#manageBookPopup').dialog('close');
		}else if(actSel == "Publish Draft"){
			$('#manageBookPopup').dialog('close');
			$('#mngCustLst').find('.shdhlt').removeClass('gbldra');
			var myString = $('#mngCustLst').find('.shdhlt').html(),
				draftItem = $('#mngCustLst').find('.shdhlt').attr('rel');
			$('#mngCustLst').find('.shdhlt').html(myString.substr(0, myString.length-7));
			$('#globalClients').find('option[rel=' + draftItem + ']').text(myString.substr(0, myString.length-7));
		}
	});
	
	lb.find('.hd h3').off('click').on('click', function(e){
		if($(this).text() == 'Segment'){
			$('#ui-id-1').trigger('click');
		}else if($(this).text() == 'Interactions'){
			//$('#ui-id-2').trigger('click');
		}else if($(this).text() == 'Opportunities'){
			$('#ui-id-3').trigger('click');
		}else if($(this).text() == 'Risk'){
			//$('#ui-id-4').trigger('click');
		}else if($(this).text() == 'Assets & Contributions'){
			//$('#ui-id-5').trigger('click');
		}else if($(this).text() == 'Transfers'){
			//$('#ui-id-7').trigger('click');
		}else if($(this).text() == 'Distribution'){
			//$('#ui-id-8').trigger('click');
		}else{
			
		}
		$infoH.addClass('hidden').removeClass('visible');
        if ($(this).closest('div').hasClass('hdexpanded') == true) {
            $(this).find('a').addClass('collapsed').removeClass('expanded');
            $(this).closest('div').next().addClass('expanded bgexpanded');
        } else {
            $(this).closest('div').next().addClass('expanded bgexpanded').siblings('.content').removeClass('expanded bgexpanded').hide();
            $(this).closest('div').next().siblings('.hd').removeClass('hdexpanded').find('a').addClass('collapsed').removeClass('expanded');
        }
	});
	
	if (userInfo == "UWC") {
        $('#clientSelector').removeClass('closed');
        $('#bookselect2').addClass('closed');
        $('#bystate').addClass('closed');
        $('#byregion').removeClass('closed');       
        $(document).on('click', '#saveCustomBook', function(e) {
            e.preventDefault();
            var $bnp = $('#bookNamePopup'),
				bnp = $('#bookNamePopup').val(),
                bnpL = bnp.length,
                lstC = bnp.substr(0, 3),
                sdb = $('#setDfltBook').is(':checked'),
                chkdRdo = $('#bookNameRadio').find('input[type=radio]:checked').attr('rel'),
                chkd = chkdRdo.split(' ');
            if ($('#radio1').is(':checked') == true) {
                if (sdb == true) {
                    $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" selected="selected" rel="' + lstC + '">' + bnp + '</option>');
                    $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
                } else {
                    $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" rel="' + lstC + '">' + bnp + '</option>');
                    $(chkd[0]).append('<li class="myBk" rel="' + lstC + '">' + bnp + '</li>');
                }
            } else if ($('#radio2').is(':checked') == true) {
                if (sdb == true) {
                    $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" selected="selected" rel="' + lstC + '">' + bnp + '</option>');
                    $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
                } else {
                   $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" rel="' + lstC + '">' + bnp + '</option>');
                    $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
                }
            } else {

            }
            if ($bnp.val() == '') {
                e.preventDefault();
                $('#alertModule1').addClass('visible').removeClass('hidden');
            } else {
                $('#createBookPopup').dialog('close');
            }
            var creatbook = $('#curSelPopup').html();
            $css.append('<ul id="' + lstC + '" class="nlist">' + creatbook + '</ul>');
            $bnp.val('');
        }); 
    } else {
        $('#clientSelector').addClass('closed');
        $('#myClients').addClass('closed');
        $('#byregion').removeClass('closed');
        $('#clientPopupSel').addClass('closed');
		$(document).on('click', '#saveCustomBook', function(e) {
            var bnp = $('#bookNamePopup').val(),
                bnpL = bnp.length,
                lstC = bnp.substr(0, 3),
                sdb = $('#setDfltBook').is(':checked'),
                chkdRdo = $('#bookNameRadio').find('input[type=radio]:checked').attr('rel'),
                chkd = chkdRdo.split(' ');
            if (sdb == true) {
                $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" selected="selected" rel="' + lstC + '">' + bnp + '</option>');
                $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
            } else {
                $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" rel="' + lstC + '">' + bnp + '</option>');
                $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
            }
            if ($('#bookNamePopup').val() == '') {
                e.preventDefault();
                $('#alertModule1').addClass('visible').removeClass('hidden');
            } else {
                $('#alertModule1').addClass('hidden').removeClass('visible');
                $('#createBookPopup').dialog('close');
				
            }
            $('#bookNamePopup').keyup(function() {
                if ($(this).val() != "") {
                    $('#alertModule1').addClass('hidden').removeClass('visible');
                }
            });
            var creatbook = $('#curSelPopup').html();
            $css.append('<ul id="' + lstC + '" class="nlist">' + creatbook + '</ul>');
            $('#bookNamePopup').val('');
        });  
    }

	(function() {
		var vendor_data = {
				series: [{
					type: 'pie',
					data: [
						['Fedilty', 34],
						['Vangard', 20],
						['Valic', 8],
						['Prudential', 26],
						['Fifth Vendor', 12]
					]
				}]
			},
			def_vendor_data = [
				['Fedilty', 34],
				['Vangard', 20],
				['Valic', 8],
				['Prudential', 26],
				['Fifth Vendor', 12],
				['Vendor6', 15],
				['Vendor7', 18],
				['Vendor8', 22],
				['Vendor9', 12],
				['Vendor10', 30]
			],
			databind = vendor_data,
			chartid = "renderPieChart_TransfersPie",
			chartP;

		function getCheckedIndex(elem) {
			var new_vendor_data = [];
			var _li = $('#vendorChatleg').find('ul').find('li').hide();

			$(elem).each(function(i) {
				if (this.checked) {
					if (new_vendor_data.length < 5) {
						new_vendor_data.push(def_vendor_data[i]);
					};
				}
			});

			new_vendor_data.map(function(obj, ind) {
				var ob = obj[0]
				_li.eq(ind).show().find('.legendFundName').html(ob)
			});
			return new_vendor_data;
		};

		chartP = new Highcharts.Chart({
			chart: {
				renderTo: chartid,
				height: 200,
				width: 300,
				padding: [0, 0, 0, 0],
				margin: [-20, 30, 30, 0],
				spacingTop: 0,
				spacingBottom: 0,
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: ''
			},
			colors: ['#0052A3', '#7F3785', '#512D6D', '#00A0AF', '#658D1B'],
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: '20',
				margin: [20, 0, 0, 0],
				borderWidth: 0
			},
			tooltip: {
				enabled: true,
				formatter: function() {
					return '$' + this.point.y + ',000,000';
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: false,
					cursor: 'pointer',
					dataLabels: {
						enabled: false
					},
					showInLegend: false
				}
			},
			credits: {
				enabled: false
			},
			series: databind.series
		});
		$(document).on('click', '.nTopven', function(e) {
			var _arr1 = getCheckedIndex($('.nTopven'));
			chartP.series[0].setData(_arr1);
		});
	})();
	
	setTimeout(function(){
		 
		 
		(function($){
				// Get all the elements and their positions
			var $container = $('.bobContainer').eq(0),
				$leftCol   = $container.find('div.firstColumn'),
				$rightCol  = $container.find('div.secondColumn'),
				$ctrl      = $container.find('#layoutCtrlLink'),				
				
				// Get the postion of the elements
				contTop    = $container.offset().top, 
				contLeft   = $container.offset().left,			
				leftTop    = $leftCol.offset().top,
				leftLeft   = $leftCol.offset().left,
				rightTop   = $rightCol.offset().top,
				rightLeft  = $rightCol.offset().left, 
				$scrollEle, scrollPos;
 
			// Setting the firstCol top postion for IE7
			if(ie <= 7){
				//$leftCol.css('top', contTop+20);
			};

			// Setting controller top position
			$ctrl.css('top', contTop);

			// Setting the height of left Column
		 
			$(window).on('resize', function(){
			 
				//$leftCol.height($(window).height() - (contTop+$('.pageTitleWrapper').height()));	 
					if(ie){ 
					  var minval=114;	
					}else
					{
					  var minval=94;
					} 
			    $leftCol.height($(window).height() - (contTop-$('.pageTitleWrapper').height() + minval)); 
				setTimeout(function(){
					var topval =parseInt($('#wrapper').css('margin-top').replace("px", ""));
					topval=topval+8; 
				// $leftCol.css('top', topval);
								
				},100);
			 
			}); 
		 $("body").on("resize", function () { 
		 scrollLeft(); 
		 }) 
			$("body").on("mouseover", function () { 
		    scrollLeft(); 
			})
 
function scrollLeft(){
 
			 		if(ie){ 
					  var minval=114;	
					}else
					{
					  var minval=94;
					} 
			    $leftCol.height($(window).height() - (contTop-$('.pageTitleWrapper').height() + minval)); 
				setTimeout(function(){
					var topval =parseInt($('#wrapper').css('margin-top').replace("px", ""));
					topval=topval+8; 
				 // $leftCol.css('top', topval);
								
				},80);
				
				$("div.firstColumn").getNiceScroll().resize();
	
}

			// Trigger the resize
			$(window).trigger('resize');

			// Enable the niceScroll for the left column
			$leftCol.niceScroll({
				horizrailenabled: false,
				autohidemode: false
			});
 
            // <a href="#" id="layoutCtrlLink" class="minusLink expanded" data-expandTxt="Expand" data-collapseTxt="Collapse">Collapse</a> 
			$ctrl.on('click', function(e){ var _ = $(this);

				if(_.parent().hasClass('expanded')){
					console.log("YEs");
					_.parent().removeClass('expanded').addClass('collapsed');
					_.html(_.attr('data-expandTxt'));
					// hideLeft();
				}else if(_.parent().hasClass('collapsed')){
					console.log("Expnded");
					_.parent().removeClass('collapsed').addClass('expanded');
					_.html(_.attr('data-collapseTxt'));
				};
			});

			// Scrolling Borrowed from our own ud_main.js. 
			$(window).on('scroll',function(e){var _timer,
				_count = 10; 

				clearTimeout(_timer);
				_timer = setTimeout(function(){
					var _scrollAmt = $(document).scrollLeft();

					// Move the left comun along with left
					$leftCol.add($ctrl).css({ "left": -_scrollAmt+20 });

					// Move the slimScroll along with scroll
					$scrollEle = $('#ascrail2000');
					if($scrollEle.length > 0){
						scrollPos = parseInt($scrollEle.css('left'));
						$scrollEle.css("left", (-_scrollAmt+scrollPos+100) + '!important'); 
					}; 
				},_count); 
			});

		})(jQuery);

	}, 400);
	
	
	
	
/* function for creating slider starts */		
function createSlider(sliderid,minval,maxval,step,val1,val2,txtbox1,txtbox2){
	var initval1=0;
	var initval2=0;
	$('#'+sliderid).slider({
		range:true,
		min:minval,
		max:maxval,
		step:step,
		values:[ val1,val2],
		start:function(event,ui){
			initval1=ui.values[ 0 ];
			initval2=ui.values[ 1 ];
		},
		stop: function( event, ui ) {		 
			if(initval1!=ui.values[ 0 ]){
			   if(ui.values[ 0 ]<1000){
					$( "#"+txtbox1 ).val( ui.values[ 0 ] );
			   }else if(ui.values[ 0 ]>=1000 && ui.values[ 0 ]<1000000){
					 $(  "#"+txtbox1 ).val( ui.values[ 0 ]/1000+"k" );
			   }else if(ui.values[ 0 ]>=1000000 ){				
					 if(ui.values[ 0 ]==5000000 ){
						$(  "#"+txtbox1 ).val( ui.values[ 0 ]/1000000+"m+" );
					 }else{
						$(  "#"+txtbox1 ).val( ui.values[ 0 ]/1000000+"m" );	 
					 }
				}	
			}		   
			if(initval2!=ui.values[ 1 ]){
			  if(ui.values[ 1 ]<1000){
					$( "#"+txtbox2 ).val( ui.values[ 1 ] );
			  }
			  else if(ui.values[ 1 ]>=1000 && ui.values[ 1 ]<1000000){
					 $(  "#"+txtbox2 ).val( ui.values[ 1 ]/1000+"k" );
			  }
			  else if(ui.values[ 1 ]>=1000000 ){				
				 if(ui.values[ 1 ]==5000000 ){
					$(  "#"+txtbox2 ).val( ui.values[ 1 ]/1000000+"m+" );
				 }
				 else{
					$(  "#"+txtbox2 ).val( ui.values[ 1 ]/1000000+"m" );
				 }
			 }
			}
		  }
    });  
}
$("#non_qualified_held_away_slider").on( "slidechange", function( event, ui ){
	var sliderIndex= $(event.target).index();
	getStepvalue(this,sliderIndex)
});
/* function used to change the step value based on slider value starts */
function getStepvalue(slidername,sliderIndex){	
	var sliderVal = $(slidername).slider( "option", "values" );
	var stepVal=50000;	
	$(slidername).slider( "option", "step", stepVal );	
}
/* function used to change the step value based on slider value ends */
/* function for creating slider starts*/	
 
	$('#qualified_held_away_slider').slider( 'disable'); // initially disabling qualified_held_away_slider
	createSlider("non_qualified_held_away_slider",0,5000000,50000,0,5000000,"range9","range10"); // non_qualified_held_away_slider
 
/* function for creating slider ends*/	    
  
  /* events for changing the slider value on text box keyup event starts*/
 
   $( "#range9,#range10" ).keyup(function() {	 
	  setSliderVal("range9","range10","non_qualified_held_away_slider",0,5000000);
   });   
 
   
   /* events for changing the slider value on text box keyup event ends*/
  /* function for changing the slider value on text box keyup event starts*/ 
  function setSliderVal(txtbox1,txtbox2,sliderid,minival,maximumval){	 
	  if($("#"+txtbox2).val()==""){
		var minval=parseFloat(minival);
	  }else{
		var minval=$("#"+txtbox1).val(); 
	  }	  
	  if($("#"+txtbox2).val()==""){
		var maxval=parseFloat(maximumval);
	  }else{
		var maxval=$("#"+txtbox2).val()  
	  }	  
	  var minvaldata1=minval.toString().split("k");
	  var minvaldata1len=minvaldata1.length;
	  var sliderminval=parseFloat(minival);
	  var slidermaxval=parseFloat(maximumval);	  
	  var minvaldata2=minval.toString().split("m");
	  var minvaldata2len=minvaldata2.length;	  
	  if(minvaldata1len==2){		
		 sliderminval=parseFloat(minvaldata1[0])*1000;
	  }else if(minvaldata2len==2){		 
		 sliderminval=parseFloat(minvaldata2[0])*1000000;		   
	  }else{		
		 sliderminval= parseFloat(minval);
	  }
	  var maxvaldata1=maxval.toString().split("k");
	  var maxvaldata1len=maxvaldata1.length;
	  var maxvaldata2=maxval.toString().split("m");
	  var maxvaldata2len=maxvaldata2.length;
	  if(maxvaldata1len==2){		
		   slidermaxval=parseFloat(maxvaldata1[0])*1000;
	  }else if(maxvaldata2len==2){		 
		   slidermaxval=parseFloat(maxvaldata2[0])*1000000;		  
	  }else{		
		 slidermaxval= parseFloat(maxval);
	  }
	  if(sliderminval>slidermaxval){
		  $('#'+sliderid).slider({					
				values:[slidermaxval,sliderminval]      
		  });	
	  }else{
		  $('#'+sliderid).slider({					
				values:[sliderminval,slidermaxval]      
		  });
	  }     
   }  
   /* function for changing the slider value on text box keyup event ends*/    
   
   var sliderDiv=new Array("infodiv","info_qualified_balance","info_non_qualified_balance","info_qualified_held_away","info_non_qualified_held_away","info_retirement_account","info_mutual_fund_account","info_self_directed_account","info_private_asset_management","info_private_asset_management1","info_prop_mf_account","info_tax_annuity","info_insurance_acc","info_immediate_annuity_acc","ageinfodiv","info_opportunities","info_activities","infodiv_account");
   
   // function used to hide other hove content when a infohover is clicked
    function showSliderHover(divval,sliderval,lblval){		
		if(sliderval=="ageframe-slider"){				
			var ageleftpos	=($('#ageframe-slider').closest('.bd').width())/2;						 
			$('#lbage1_mid').css('left',ageleftpos+26);   			
			var lblOffsetLen=$( "#"+sliderval).closest('.bd').width();			
      	    $( "#"+lblval ).css('left',lblOffsetLen);	
		}
		else{
		 var lblOffsetLen=$( "#"+sliderval).closest('.bd').width();	
		 if(sliderval=="opportunities_slider" || sliderval=="prop_mf_account_slider" || sliderval=="aftertax_ann_slider" || sliderval=="self_directed_account_slider" || sliderval=="qualified_held_away_slider" || sliderval=="insurance_acc_slider"){
			$( "#"+lblval ).css('left','250px'); 
		 }else if(sliderval=="timeframe-sliderAcc"){
			$( "#"+lblval ).css('left','233px'); 
		 }else{
        	$( "#"+lblval ).css('left',lblOffsetLen-5);
		 }
		}
       if(sliderval!="opportunities_slider"){
		for(var k=1;k<=4;k++){
			var lblvalnew=lblval+"_"+k;
			if(k==1){
			lblOffsetLennew=60*k;
			}
			else if(k==2){
			lblOffsetLennew=55*k;
			}
			else if(k==4){
			lblOffsetLennew=52*k;
			}
			else{
			lblOffsetLennew=53*k;
			}			
			$( "#"+lblvalnew).css('left',lblOffsetLennew);
		 }
	   }
	   else{
		 for(var k=1;k<=4;k++){
			var lblvalnew=lblval+"_"+k;
			if(k==1){
			lblOffsetLennew=60*k;
			}
			else if(k==2){
			lblOffsetLennew=55*k;
			}
			else if(k==4){
			lblOffsetLennew=52*k;
			}
			else{
			lblOffsetLennew=53*k;
			}	
			var offsetval=$('#'+sliderval).offset().left;		
			var lblOffsetvalLeft=lblOffsetLennew;			
			$( "#"+lblvalnew).css('left',lblOffsetvalLeft);
		 }  
	   }
				for( var i=0;i<sliderDiv.length;i++){
					if(sliderDiv[i]!=divval){
						hideInfoHover(this,'#'+sliderDiv[i]); 
					}					
				 }
    } 
		 
		 $("body").on("click", "#totalassetsedit", function(e){
			e.stopPropagation();

			showInfoHover(this,'#infodiv');
			showSliderHover('infodiv','timeframe-slider','lbl2');	
			var oppDivWidth = 324;
			if($('#advancedFilt1').is(':checked')){
				oppDivWidth = 625;
			}			
			$('#infodiv').css('width',oppDivWidth);
			if( $('#infodiv').find('iframe').length==0){
			if(ie7Flag){
			setTimeout( function(){
				var sf = $('#infodiv'),_h = 0, _w = 0, _iframe=0;								
				_h   = $(sf).outerHeight();  
				_w   = $(sf).outerWidth();
				_iframe = $('<iframe />', {'name': 'myFrame', 'id':   'myFrame', 'class': 'ie6SelectOverlap', 'src':'javascript:false', height: _h, width: _w });
				($(sf).find('.bd').length) ? $(sf).find('.bd').prepend(_iframe) : $(sf).prepend(_iframe);   			
			}, 50);
			}
			}	
		 }); 
 
		$("body").on("click", "#qual_held_away", function(e){
			e.stopPropagation();                                      
			showInfoHover(this,'#info_qualified_held_away');
			showSliderHover('info_qualified_held_away','qualified_held_away_slider','lbl8');
			$('#info_qualified_held_away').css('width','580px');
		});
		$("body").on("click", "#non_qual_held_away", function(e){
			e.stopPropagation();                                      
			showInfoHover(this,'#info_non_qualified_held_away');
			showSliderHover('info_non_qualified_held_away','non_qualified_held_away_slider','lbl10');
			 
		});
		 
		$("body").on("click", "#oppor_link3", function(e){
			   e.stopPropagation();                                      
			   showInfoHover(this,'#ixiassets');
			   showSliderHover('ixiassets','non_qualified_held_away_slider','lbl10');
			 
			   $('#investmentId').removeClass('visible');
			   $('#assetClassId').removeClass('visible'); 
			   $('#investmentId').addClass('closed');
			   $('#assetClassId').addClass('closed');  
               $('#transferVendor').addClass('hidden');
			    
		});
		 
		var curTop = 0;
		$("body").on("click", "#self_directed_acc", function(e){			
			e.stopPropagation();                                      
			showInfoHovernew(this,'#info_self_directed_account');
			if(ie6Flag){		
				var actiDivWidth=715;
			}else{
				var actiDivWidth=700;	
			}
			showSliderHover('info_self_directed_account','self_directed_account_slider','lbl16');          
			showSliderHover('info_self_directed_account','self_directed_account_slider1','lbl16');
			$('#info_self_directed_account').css('width',actiDivWidth);			
			curTop = $('#info_self_directed_account').offset().top;			
			if( $('#info_self_directed_account').find('iframe').length==0){
			if(ie7Flag){
			setTimeout( function(){
				var sf = $('#info_self_directed_account'),_h = 0, _w = 0, _iframe=0;								
				_h   = $(sf).outerHeight();  
				_w   = $(sf).outerWidth();
				_iframe = $('<iframe />', {'name': 'myFrame', 'id':   'myFrame', 'class': 'ie6SelectOverlap', 'src':'javascript:false', height: _h, width: _w });
				($(sf).find('.bd').length) ? $(sf).find('.bd').prepend(_iframe) : $(sf).prepend(_iframe);   			
			}, 50);
			}
			}
			
		});
		 
		$("body").on("click", "#oppor_link", function(e){
			e.preventDefault();
			e.stopPropagation();                                      
			showInfoHovernew(this,'#info_opportunities');
			showSliderHover('info_opportunities','opportunities_slider','lbl28');
			var oppDivWidth=605;
			$('#info_opportunities').css('width',oppDivWidth);
			if( $('#info_opportunities').find('iframe').length==0){
			if(ie7Flag){
			setTimeout( function(){
				var sf = $('#info_opportunities'),_h = 0, _w = 0, _iframe=0;								
				_h   = $(sf).outerHeight();  
				_w   = $(sf).outerWidth();
				_iframe = $('<iframe />', {'name': 'myFrame', 'id':   'myFrame', 'class': 'ie6SelectOverlap', 'src':'javascript:false', height: _h, width: _w });
				($(sf).find('.bd').length) ? $(sf).find('.bd').prepend(_iframe) : $(sf).prepend(_iframe);   			
			}, 50);
			}
			}
	    });
		$("body").on("click", "#activities_link", function(e){
			e.preventDefault();
			e.stopPropagation();                                      
			showInfoHovernew(this,'#info_activities');
			if(ie6Flag){		
			var actiDivWidth=790;
			}else{
			var actiDivWidth=775;	
			}
			$('#info_activities').css('width',actiDivWidth);
			if( $('#info_activities').find('iframe').length==0){
				if(ie7Flag){
				setTimeout( function(){
					var sf = $('#info_activities'),_h = 0, _w = 0, _iframe=0;								
					_h   = $(sf).outerHeight();  
					_w   = $(sf).outerWidth();
					_iframe = $('<iframe />', {'name': 'myFrame', 'id':   'myFrame', 'class': 'ie6SelectOverlap', 'src':'javascript:false', height: _h, width: _w });
					($(sf).find('.bd').length) ? $(sf).find('.bd').prepend(_iframe) : $(sf).prepend(_iframe);   			
				}, 50);
				}	
			}
			for( var i=0;i<sliderDiv.length;i++){
				if(sliderDiv[i]!="info_activities"){
					hideInfoHover(this,'#'+sliderDiv[i]); 
				}					
			}		
	    });		
		  
	function hideInfoHover(objl, objdiv) {
	$(objdiv).fadeOut('fast');
	$(objdiv).removeClass('visible');
    } 
		function updatefiltercount(thisele){
		var filterCount= thisele.closest('.content').prev().find('.filterval').html();
		filterCount=parseInt(filterCount);
		filterCount=filterCount+1;		
		thisele.closest('.content').prev().find('.filterval').html(filterCount);
	}
});




//$(document).on('headLoaded', function(){
//	(console) && console.log("headLoaded",arguments);
//});
//$(document).on('titleLoaded', function(){
//	(console) && console.log("titleLoaded",arguments);
//});
//$(document).on('leftLoaded', function(){
//	(console) && console.log("leftLoaded",arguments);
//});
//$(document).on('popupsLoaded', function(){
//	(console) && console.log("popupsLoaded",arguments);
//});